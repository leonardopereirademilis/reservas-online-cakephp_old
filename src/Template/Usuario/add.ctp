<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Usuario'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Convite'), ['controller' => 'Convite', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Convite'), ['controller' => 'Convite', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Plano'), ['controller' => 'Plano', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Plano'), ['controller' => 'Plano', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Reserva'), ['controller' => 'Reserva', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Reserva'), ['controller' => 'Reserva', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Papel'), ['controller' => 'Papel', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Papel'), ['controller' => 'Papel', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usuario form large-9 medium-8 columns content">
    <?= $this->Form->create($usuario) ?>
    <fieldset>
        <legend><?= __('Add Usuario') ?></legend>
        <?php
            echo $this->Form->input('account_expired');
            echo $this->Form->input('account_locked');
            echo $this->Form->input('email');
            echo $this->Form->input('enabled');
            echo $this->Form->input('nome');
            echo $this->Form->input('password');
            echo $this->Form->input('password_expired');
            echo $this->Form->input('username');
            echo $this->Form->input('condominio._ids', ['options' => $condominio]);
            echo $this->Form->input('papel._ids', ['options' => $papel]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
