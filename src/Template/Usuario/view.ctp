<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Usuario'), ['action' => 'edit', $usuario->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Usuario'), ['action' => 'delete', $usuario->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usuario->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Usuario'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Usuario'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Convite'), ['controller' => 'Convite', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Convite'), ['controller' => 'Convite', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Plano'), ['controller' => 'Plano', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Plano'), ['controller' => 'Plano', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Reserva'), ['controller' => 'Reserva', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Reserva'), ['controller' => 'Reserva', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Papel'), ['controller' => 'Papel', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Papel'), ['controller' => 'Papel', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usuario view large-9 medium-8 columns content">
    <h3><?= h($usuario->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($usuario->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($usuario->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($usuario->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($usuario->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($usuario->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Account Expired') ?></h4>
        <?= $this->Text->autoParagraph(h($usuario->account_expired)); ?>
    </div>
    <div class="row">
        <h4><?= __('Account Locked') ?></h4>
        <?= $this->Text->autoParagraph(h($usuario->account_locked)); ?>
    </div>
    <div class="row">
        <h4><?= __('Enabled') ?></h4>
        <?= $this->Text->autoParagraph(h($usuario->enabled)); ?>
    </div>
    <div class="row">
        <h4><?= __('Password Expired') ?></h4>
        <?= $this->Text->autoParagraph(h($usuario->password_expired)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Convite') ?></h4>
        <?php if (!empty($usuario->convite)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Apartamento Id') ?></th>
                <th scope="col"><?= __('Data Aceite') ?></th>
                <th scope="col"><?= __('Data Convite') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Usuario Id') ?></th>
                <th scope="col"><?= __('Usuario Solicitou') ?></th>
                <th scope="col"><?= __('Aprovado') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($usuario->convite as $convite): ?>
            <tr>
                <td><?= h($convite->id) ?></td>
                <td><?= h($convite->apartamento_id) ?></td>
                <td><?= h($convite->data_aceite) ?></td>
                <td><?= h($convite->data_convite) ?></td>
                <td><?= h($convite->email) ?></td>
                <td><?= h($convite->usuario_id) ?></td>
                <td><?= h($convite->usuario_solicitou) ?></td>
                <td><?= h($convite->aprovado) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Convite', 'action' => 'view', $convite->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Convite', 'action' => 'edit', $convite->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Convite', 'action' => 'delete', $convite->id], ['confirm' => __('Are you sure you want to delete # {0}?', $convite->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Plano') ?></h4>
        <?php if (!empty($usuario->plano)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ativo') ?></th>
                <th scope="col"><?= __('Data Fim') ?></th>
                <th scope="col"><?= __('Data Inicio') ?></th>
                <th scope="col"><?= __('Tipo Plano Id') ?></th>
                <th scope="col"><?= __('Usuario Id') ?></th>
                <th scope="col"><?= __('Planos Idx') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($usuario->plano as $plano): ?>
            <tr>
                <td><?= h($plano->id) ?></td>
                <td><?= h($plano->ativo) ?></td>
                <td><?= h($plano->data_fim) ?></td>
                <td><?= h($plano->data_inicio) ?></td>
                <td><?= h($plano->tipo_plano_id) ?></td>
                <td><?= h($plano->usuario_id) ?></td>
                <td><?= h($plano->planos_idx) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Plano', 'action' => 'view', $plano->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Plano', 'action' => 'edit', $plano->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Plano', 'action' => 'delete', $plano->id], ['confirm' => __('Are you sure you want to delete # {0}?', $plano->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Reserva') ?></h4>
        <?php if (!empty($usuario->reserva)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Apartamento Id') ?></th>
                <th scope="col"><?= __('Aprovada') ?></th>
                <th scope="col"><?= __('Cancelada') ?></th>
                <th scope="col"><?= __('Comentario') ?></th>
                <th scope="col"><?= __('Data Aprovacao') ?></th>
                <th scope="col"><?= __('Data Cancelamento') ?></th>
                <th scope="col"><?= __('Data Solicitacao') ?></th>
                <th scope="col"><?= __('Recurso Id') ?></th>
                <th scope="col"><?= __('Usuario Id') ?></th>
                <th scope="col"><?= __('Valor') ?></th>
                <th scope="col"><?= __('Data Fim Evento') ?></th>
                <th scope="col"><?= __('Data Inicio Evento') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($usuario->reserva as $reserva): ?>
            <tr>
                <td><?= h($reserva->id) ?></td>
                <td><?= h($reserva->apartamento_id) ?></td>
                <td><?= h($reserva->aprovada) ?></td>
                <td><?= h($reserva->cancelada) ?></td>
                <td><?= h($reserva->comentario) ?></td>
                <td><?= h($reserva->data_aprovacao) ?></td>
                <td><?= h($reserva->data_cancelamento) ?></td>
                <td><?= h($reserva->data_solicitacao) ?></td>
                <td><?= h($reserva->recurso_id) ?></td>
                <td><?= h($reserva->usuario_id) ?></td>
                <td><?= h($reserva->valor) ?></td>
                <td><?= h($reserva->data_fim_evento) ?></td>
                <td><?= h($reserva->data_inicio_evento) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Reserva', 'action' => 'view', $reserva->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Reserva', 'action' => 'edit', $reserva->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Reserva', 'action' => 'delete', $reserva->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reserva->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Condominio') ?></h4>
        <?php if (!empty($usuario->condominio)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Endereco Id') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Plano Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($usuario->condominio as $condominio): ?>
            <tr>
                <td><?= h($condominio->id) ?></td>
                <td><?= h($condominio->endereco_id) ?></td>
                <td><?= h($condominio->nome) ?></td>
                <td><?= h($condominio->plano_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Condominio', 'action' => 'view', $condominio->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Condominio', 'action' => 'edit', $condominio->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Condominio', 'action' => 'delete', $condominio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $condominio->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Papel') ?></h4>
        <?php if (!empty($usuario->papel)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Authority') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($usuario->papel as $papel): ?>
            <tr>
                <td><?= h($papel->id) ?></td>
                <td><?= h($papel->authority) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Papel', 'action' => 'view', $papel->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Papel', 'action' => 'edit', $papel->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Papel', 'action' => 'delete', $papel->id], ['confirm' => __('Are you sure you want to delete # {0}?', $papel->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
