<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $condominio->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $condominio->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Condominio'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Endereco'), ['controller' => 'Endereco', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Endereco'), ['controller' => 'Endereco', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Plano'), ['controller' => 'Plano', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Plano'), ['controller' => 'Plano', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Apartamento'), ['controller' => 'Apartamento', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Apartamento'), ['controller' => 'Apartamento', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Convite'), ['controller' => 'Convite', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Convite'), ['controller' => 'Convite', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="condominio form large-9 medium-8 columns content">
    <?= $this->Form->create($condominio) ?>
    <fieldset>
        <legend><?= __('Edit Condominio') ?></legend>
        <?php
            echo $this->Form->input('endereco_id', ['options' => $endereco]);
            echo $this->Form->input('nome');
            echo $this->Form->input('plano_id', ['options' => $plano]);
            echo $this->Form->input('convite._ids', ['options' => $convite]);
            echo $this->Form->input('usuario._ids', ['options' => $usuario]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
