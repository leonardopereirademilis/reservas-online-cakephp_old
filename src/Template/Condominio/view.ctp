<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Condominio'), ['action' => 'edit', $condominio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Condominio'), ['action' => 'delete', $condominio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $condominio->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Condominio'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Condominio'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Endereco'), ['controller' => 'Endereco', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Endereco'), ['controller' => 'Endereco', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Plano'), ['controller' => 'Plano', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Plano'), ['controller' => 'Plano', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Apartamento'), ['controller' => 'Apartamento', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Apartamento'), ['controller' => 'Apartamento', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Convite'), ['controller' => 'Convite', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Convite'), ['controller' => 'Convite', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="condominio view large-9 medium-8 columns content">
    <h3><?= h($condominio->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Endereco') ?></th>
            <td><?= $condominio->has('endereco') ? $this->Html->link($condominio->endereco->id, ['controller' => 'Endereco', 'action' => 'view', $condominio->endereco->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($condominio->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Plano') ?></th>
            <td><?= $condominio->has('plano') ? $this->Html->link($condominio->plano->id, ['controller' => 'Plano', 'action' => 'view', $condominio->plano->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($condominio->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Apartamento') ?></h4>
        <?php if (!empty($condominio->apartamento)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Bloco') ?></th>
                <th scope="col"><?= __('Condominio Id') ?></th>
                <th scope="col"><?= __('Numero') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($condominio->apartamento as $apartamento): ?>
            <tr>
                <td><?= h($apartamento->id) ?></td>
                <td><?= h($apartamento->bloco) ?></td>
                <td><?= h($apartamento->condominio_id) ?></td>
                <td><?= h($apartamento->numero) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Apartamento', 'action' => 'view', $apartamento->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Apartamento', 'action' => 'edit', $apartamento->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Apartamento', 'action' => 'delete', $apartamento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $apartamento->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Mensalidade') ?></h4>
        <?php if (!empty($condominio->mensalidade)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ano') ?></th>
                <th scope="col"><?= __('Condominio Id') ?></th>
                <th scope="col"><?= __('Mes') ?></th>
                <th scope="col"><?= __('Plano Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($condominio->mensalidade as $mensalidade): ?>
            <tr>
                <td><?= h($mensalidade->id) ?></td>
                <td><?= h($mensalidade->ano) ?></td>
                <td><?= h($mensalidade->condominio_id) ?></td>
                <td><?= h($mensalidade->mes) ?></td>
                <td><?= h($mensalidade->plano_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Mensalidade', 'action' => 'view', $mensalidade->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Mensalidade', 'action' => 'edit', $mensalidade->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Mensalidade', 'action' => 'delete', $mensalidade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mensalidade->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Recurso') ?></h4>
        <?php if (!empty($condominio->recurso)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ativo') ?></th>
                <th scope="col"><?= __('Capacidade') ?></th>
                <th scope="col"><?= __('Condominio Id') ?></th>
                <th scope="col"><?= __('Descricao') ?></th>
                <th scope="col"><?= __('Exige Confirmacao') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Numero Max Reservas') ?></th>
                <th scope="col"><?= __('Tempo Reserva') ?></th>
                <th scope="col"><?= __('Valor') ?></th>
                <th scope="col"><?= __('Unidade Tempo Reserva Id') ?></th>
                <th scope="col"><?= __('Cor Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($condominio->recurso as $recurso): ?>
            <tr>
                <td><?= h($recurso->id) ?></td>
                <td><?= h($recurso->ativo) ?></td>
                <td><?= h($recurso->capacidade) ?></td>
                <td><?= h($recurso->condominio_id) ?></td>
                <td><?= h($recurso->descricao) ?></td>
                <td><?= h($recurso->exige_confirmacao) ?></td>
                <td><?= h($recurso->nome) ?></td>
                <td><?= h($recurso->numero_max_reservas) ?></td>
                <td><?= h($recurso->tempo_reserva) ?></td>
                <td><?= h($recurso->valor) ?></td>
                <td><?= h($recurso->unidade_tempo_reserva_id) ?></td>
                <td><?= h($recurso->cor_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Recurso', 'action' => 'view', $recurso->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Recurso', 'action' => 'edit', $recurso->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Recurso', 'action' => 'delete', $recurso->id], ['confirm' => __('Are you sure you want to delete # {0}?', $recurso->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Convite') ?></h4>
        <?php if (!empty($condominio->convite)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Apartamento Id') ?></th>
                <th scope="col"><?= __('Data Aceite') ?></th>
                <th scope="col"><?= __('Data Convite') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Usuario Id') ?></th>
                <th scope="col"><?= __('Usuario Solicitou') ?></th>
                <th scope="col"><?= __('Aprovado') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($condominio->convite as $convite): ?>
            <tr>
                <td><?= h($convite->id) ?></td>
                <td><?= h($convite->apartamento_id) ?></td>
                <td><?= h($convite->data_aceite) ?></td>
                <td><?= h($convite->data_convite) ?></td>
                <td><?= h($convite->email) ?></td>
                <td><?= h($convite->usuario_id) ?></td>
                <td><?= h($convite->usuario_solicitou) ?></td>
                <td><?= h($convite->aprovado) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Convite', 'action' => 'view', $convite->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Convite', 'action' => 'edit', $convite->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Convite', 'action' => 'delete', $convite->id], ['confirm' => __('Are you sure you want to delete # {0}?', $convite->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Usuario') ?></h4>
        <?php if (!empty($condominio->usuario)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Account Expired') ?></th>
                <th scope="col"><?= __('Account Locked') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Enabled') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col"><?= __('Password Expired') ?></th>
                <th scope="col"><?= __('Username') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($condominio->usuario as $usuario): ?>
            <tr>
                <td><?= h($usuario->id) ?></td>
                <td><?= h($usuario->account_expired) ?></td>
                <td><?= h($usuario->account_locked) ?></td>
                <td><?= h($usuario->email) ?></td>
                <td><?= h($usuario->enabled) ?></td>
                <td><?= h($usuario->nome) ?></td>
                <td><?= h($usuario->password) ?></td>
                <td><?= h($usuario->password_expired) ?></td>
                <td><?= h($usuario->username) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Usuario', 'action' => 'view', $usuario->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Usuario', 'action' => 'edit', $usuario->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Usuario', 'action' => 'delete', $usuario->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usuario->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
