<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Mensalidade'), ['action' => 'edit', $mensalidade->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Mensalidade'), ['action' => 'delete', $mensalidade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mensalidade->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Mensalidade'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mensalidade'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Plano'), ['controller' => 'Plano', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Plano'), ['controller' => 'Plano', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Boleto'), ['controller' => 'Boleto', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Boleto'), ['controller' => 'Boleto', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="mensalidade view large-9 medium-8 columns content">
    <h3><?= h($mensalidade->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Condominio') ?></th>
            <td><?= $mensalidade->has('condominio') ? $this->Html->link($mensalidade->condominio->id, ['controller' => 'Condominio', 'action' => 'view', $mensalidade->condominio->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Plano') ?></th>
            <td><?= $mensalidade->has('plano') ? $this->Html->link($mensalidade->plano->id, ['controller' => 'Plano', 'action' => 'view', $mensalidade->plano->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($mensalidade->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ano') ?></th>
            <td><?= $this->Number->format($mensalidade->ano) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mes') ?></th>
            <td><?= $this->Number->format($mensalidade->mes) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Boleto') ?></h4>
        <?php if (!empty($mensalidade->boleto)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Codigo') ?></th>
                <th scope="col"><?= __('Data Geracao') ?></th>
                <th scope="col"><?= __('Data Pagamento') ?></th>
                <th scope="col"><?= __('Data Vencimento') ?></th>
                <th scope="col"><?= __('Mensalidade Id') ?></th>
                <th scope="col"><?= __('Pago') ?></th>
                <th scope="col"><?= __('Valor') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($mensalidade->boleto as $boleto): ?>
            <tr>
                <td><?= h($boleto->id) ?></td>
                <td><?= h($boleto->codigo) ?></td>
                <td><?= h($boleto->data_geracao) ?></td>
                <td><?= h($boleto->data_pagamento) ?></td>
                <td><?= h($boleto->data_vencimento) ?></td>
                <td><?= h($boleto->mensalidade_id) ?></td>
                <td><?= h($boleto->pago) ?></td>
                <td><?= h($boleto->valor) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Boleto', 'action' => 'view', $boleto->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Boleto', 'action' => 'edit', $boleto->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Boleto', 'action' => 'delete', $boleto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $boleto->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
