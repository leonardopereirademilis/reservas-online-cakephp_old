<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Mensalidade'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Plano'), ['controller' => 'Plano', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Plano'), ['controller' => 'Plano', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Boleto'), ['controller' => 'Boleto', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Boleto'), ['controller' => 'Boleto', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="mensalidade index large-9 medium-8 columns content">
    <h3><?= __('Mensalidade') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ano') ?></th>
                <th scope="col"><?= $this->Paginator->sort('condominio_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mes') ?></th>
                <th scope="col"><?= $this->Paginator->sort('plano_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($mensalidade as $mensalidade): ?>
            <tr>
                <td><?= $this->Number->format($mensalidade->id) ?></td>
                <td><?= $this->Number->format($mensalidade->ano) ?></td>
                <td><?= $mensalidade->has('condominio') ? $this->Html->link($mensalidade->condominio->id, ['controller' => 'Condominio', 'action' => 'view', $mensalidade->condominio->id]) : '' ?></td>
                <td><?= $this->Number->format($mensalidade->mes) ?></td>
                <td><?= $mensalidade->has('plano') ? $this->Html->link($mensalidade->plano->id, ['controller' => 'Plano', 'action' => 'view', $mensalidade->plano->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $mensalidade->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mensalidade->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mensalidade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mensalidade->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
