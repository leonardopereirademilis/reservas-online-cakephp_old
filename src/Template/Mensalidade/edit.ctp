<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $mensalidade->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $mensalidade->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Mensalidade'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Plano'), ['controller' => 'Plano', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Plano'), ['controller' => 'Plano', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Boleto'), ['controller' => 'Boleto', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Boleto'), ['controller' => 'Boleto', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="mensalidade form large-9 medium-8 columns content">
    <?= $this->Form->create($mensalidade) ?>
    <fieldset>
        <legend><?= __('Edit Mensalidade') ?></legend>
        <?php
            echo $this->Form->input('ano');
            echo $this->Form->input('condominio_id', ['options' => $condominio]);
            echo $this->Form->input('mes');
            echo $this->Form->input('plano_id', ['options' => $plano]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
