<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Boleto'), ['action' => 'edit', $boleto->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Boleto'), ['action' => 'delete', $boleto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $boleto->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Boleto'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Boleto'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="boleto view large-9 medium-8 columns content">
    <h3><?= h($boleto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Codigo') ?></th>
            <td><?= h($boleto->codigo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mensalidade') ?></th>
            <td><?= $boleto->has('mensalidade') ? $this->Html->link($boleto->mensalidade->id, ['controller' => 'Mensalidade', 'action' => 'view', $boleto->mensalidade->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($boleto->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Valor') ?></th>
            <td><?= $this->Number->format($boleto->valor) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Geracao') ?></th>
            <td><?= h($boleto->data_geracao) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Pagamento') ?></th>
            <td><?= h($boleto->data_pagamento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Vencimento') ?></th>
            <td><?= h($boleto->data_vencimento) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Pago') ?></h4>
        <?= $this->Text->autoParagraph(h($boleto->pago)); ?>
    </div>
</div>
