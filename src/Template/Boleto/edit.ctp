<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $boleto->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $boleto->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Boleto'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="boleto form large-9 medium-8 columns content">
    <?= $this->Form->create($boleto) ?>
    <fieldset>
        <legend><?= __('Edit Boleto') ?></legend>
        <?php
            echo $this->Form->input('codigo');
            echo $this->Form->input('data_geracao');
            echo $this->Form->input('data_pagamento', ['empty' => true]);
            echo $this->Form->input('data_vencimento');
            echo $this->Form->input('mensalidade_id', ['options' => $mensalidade]);
            echo $this->Form->input('pago');
            echo $this->Form->input('valor');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
