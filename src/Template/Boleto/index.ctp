<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Boleto'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="boleto index large-9 medium-8 columns content">
    <h3><?= __('Boleto') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('codigo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_geracao') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_pagamento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_vencimento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mensalidade_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('valor') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($boleto as $boleto): ?>
            <tr>
                <td><?= $this->Number->format($boleto->id) ?></td>
                <td><?= h($boleto->codigo) ?></td>
                <td><?= h($boleto->data_geracao) ?></td>
                <td><?= h($boleto->data_pagamento) ?></td>
                <td><?= h($boleto->data_vencimento) ?></td>
                <td><?= $boleto->has('mensalidade') ? $this->Html->link($boleto->mensalidade->id, ['controller' => 'Mensalidade', 'action' => 'view', $boleto->mensalidade->id]) : '' ?></td>
                <td><?= $this->Number->format($boleto->valor) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $boleto->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $boleto->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $boleto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $boleto->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
