<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $cor->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $cor->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Cor'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cor form large-9 medium-8 columns content">
    <?= $this->Form->create($cor) ?>
    <fieldset>
        <legend><?= __('Edit Cor') ?></legend>
        <?php
            echo $this->Form->input('cor');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
