<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Papel'), ['action' => 'edit', $papel->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Papel'), ['action' => 'delete', $papel->id], ['confirm' => __('Are you sure you want to delete # {0}?', $papel->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Papel'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Papel'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="papel view large-9 medium-8 columns content">
    <h3><?= h($papel->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Authority') ?></th>
            <td><?= h($papel->authority) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($papel->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Usuario') ?></h4>
        <?php if (!empty($papel->usuario)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Account Expired') ?></th>
                <th scope="col"><?= __('Account Locked') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Enabled') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col"><?= __('Password Expired') ?></th>
                <th scope="col"><?= __('Username') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($papel->usuario as $usuario): ?>
            <tr>
                <td><?= h($usuario->id) ?></td>
                <td><?= h($usuario->account_expired) ?></td>
                <td><?= h($usuario->account_locked) ?></td>
                <td><?= h($usuario->email) ?></td>
                <td><?= h($usuario->enabled) ?></td>
                <td><?= h($usuario->nome) ?></td>
                <td><?= h($usuario->password) ?></td>
                <td><?= h($usuario->password_expired) ?></td>
                <td><?= h($usuario->username) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Usuario', 'action' => 'view', $usuario->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Usuario', 'action' => 'edit', $usuario->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Usuario', 'action' => 'delete', $usuario->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usuario->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
