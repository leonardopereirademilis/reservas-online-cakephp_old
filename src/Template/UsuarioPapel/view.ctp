<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Usuario Papel'), ['action' => 'edit', $usuarioPapel->papel_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Usuario Papel'), ['action' => 'delete', $usuarioPapel->papel_id], ['confirm' => __('Are you sure you want to delete # {0}?', $usuarioPapel->papel_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Usuario Papel'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Usuario Papel'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Papel'), ['controller' => 'Papel', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Papel'), ['controller' => 'Papel', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usuarioPapel view large-9 medium-8 columns content">
    <h3><?= h($usuarioPapel->papel_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Papel') ?></th>
            <td><?= $usuarioPapel->has('papel') ? $this->Html->link($usuarioPapel->papel->id, ['controller' => 'Papel', 'action' => 'view', $usuarioPapel->papel->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usuario') ?></th>
            <td><?= $usuarioPapel->has('usuario') ? $this->Html->link($usuarioPapel->usuario->id, ['controller' => 'Usuario', 'action' => 'view', $usuarioPapel->usuario->id]) : '' ?></td>
        </tr>
    </table>
</div>
