<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Usuario Papel'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Papel'), ['controller' => 'Papel', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Papel'), ['controller' => 'Papel', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usuarioPapel index large-9 medium-8 columns content">
    <h3><?= __('Usuario Papel') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('papel_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('usuario_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usuarioPapel as $usuarioPapel): ?>
            <tr>
                <td><?= $usuarioPapel->has('papel') ? $this->Html->link($usuarioPapel->papel->id, ['controller' => 'Papel', 'action' => 'view', $usuarioPapel->papel->id]) : '' ?></td>
                <td><?= $usuarioPapel->has('usuario') ? $this->Html->link($usuarioPapel->usuario->id, ['controller' => 'Usuario', 'action' => 'view', $usuarioPapel->usuario->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $usuarioPapel->papel_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $usuarioPapel->papel_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $usuarioPapel->papel_id], ['confirm' => __('Are you sure you want to delete # {0}?', $usuarioPapel->papel_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
