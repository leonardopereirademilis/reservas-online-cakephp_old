<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Pais'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="pais form large-9 medium-8 columns content">
    <?= $this->Form->create($pai) ?>
    <fieldset>
        <legend><?= __('Add Pai') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('nome');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
