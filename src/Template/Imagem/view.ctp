<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Imagem'), ['action' => 'edit', $imagem->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Imagem'), ['action' => 'delete', $imagem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $imagem->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Imagem'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Imagem'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="imagem view large-9 medium-8 columns content">
    <h3><?= h($imagem->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Recurso') ?></th>
            <td><?= $imagem->has('recurso') ? $this->Html->link($imagem->recurso->id, ['controller' => 'Recurso', 'action' => 'view', $imagem->recurso->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($imagem->id) ?></td>
        </tr>
    </table>
</div>
