<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $imagem->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $imagem->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Imagem'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="imagem form large-9 medium-8 columns content">
    <?= $this->Form->create($imagem) ?>
    <fieldset>
        <legend><?= __('Edit Imagem') ?></legend>
        <?php
            echo $this->Form->input('recurso_id', ['options' => $recurso]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
