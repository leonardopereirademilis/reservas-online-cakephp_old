<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Imagem'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="imagem index large-9 medium-8 columns content">
    <h3><?= __('Imagem') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('recurso_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($imagem as $imagem): ?>
            <tr>
                <td><?= $this->Number->format($imagem->id) ?></td>
                <td><?= $imagem->has('recurso') ? $this->Html->link($imagem->recurso->id, ['controller' => 'Recurso', 'action' => 'view', $imagem->recurso->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $imagem->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $imagem->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $imagem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $imagem->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
