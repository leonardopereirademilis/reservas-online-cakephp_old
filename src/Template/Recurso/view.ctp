<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Recurso'), ['action' => 'edit', $recurso->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Recurso'), ['action' => 'delete', $recurso->id], ['confirm' => __('Are you sure you want to delete # {0}?', $recurso->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Recurso'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recurso'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Unidade Tempo Reserva'), ['controller' => 'UnidadeTempoReserva', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Unidade Tempo Reserva'), ['controller' => 'UnidadeTempoReserva', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cor'), ['controller' => 'Cor', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cor'), ['controller' => 'Cor', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Imagem'), ['controller' => 'Imagem', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Imagem'), ['controller' => 'Imagem', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Indisponibilidade'), ['controller' => 'Indisponibilidade', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Indisponibilidade'), ['controller' => 'Indisponibilidade', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Reserva'), ['controller' => 'Reserva', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Reserva'), ['controller' => 'Reserva', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tipo Reserva'), ['controller' => 'TipoReserva', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tipo Reserva'), ['controller' => 'TipoReserva', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="recurso view large-9 medium-8 columns content">
    <h3><?= h($recurso->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Condominio') ?></th>
            <td><?= $recurso->has('condominio') ? $this->Html->link($recurso->condominio->id, ['controller' => 'Condominio', 'action' => 'view', $recurso->condominio->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($recurso->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Unidade Tempo Reserva') ?></th>
            <td><?= $recurso->has('unidade_tempo_reserva') ? $this->Html->link($recurso->unidade_tempo_reserva->id, ['controller' => 'UnidadeTempoReserva', 'action' => 'view', $recurso->unidade_tempo_reserva->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cor') ?></th>
            <td><?= $recurso->has('cor') ? $this->Html->link($recurso->cor->id, ['controller' => 'Cor', 'action' => 'view', $recurso->cor->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($recurso->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Capacidade') ?></th>
            <td><?= $this->Number->format($recurso->capacidade) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Numero Max Reservas') ?></th>
            <td><?= $this->Number->format($recurso->numero_max_reservas) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tempo Reserva') ?></th>
            <td><?= $this->Number->format($recurso->tempo_reserva) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Valor') ?></th>
            <td><?= $this->Number->format($recurso->valor) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Ativo') ?></h4>
        <?= $this->Text->autoParagraph(h($recurso->ativo)); ?>
    </div>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($recurso->descricao)); ?>
    </div>
    <div class="row">
        <h4><?= __('Exige Confirmacao') ?></h4>
        <?= $this->Text->autoParagraph(h($recurso->exige_confirmacao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Imagem') ?></h4>
        <?php if (!empty($recurso->imagem)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Imagem') ?></th>
                <th scope="col"><?= __('Recurso Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($recurso->imagem as $imagem): ?>
            <tr>
                <td><?= h($imagem->id) ?></td>
                <td><?= h($imagem->imagem) ?></td>
                <td><?= h($imagem->recurso_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Imagem', 'action' => 'view', $imagem->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Imagem', 'action' => 'edit', $imagem->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Imagem', 'action' => 'delete', $imagem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $imagem->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Indisponibilidade') ?></h4>
        <?php if (!empty($recurso->indisponibilidade)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ativo') ?></th>
                <th scope="col"><?= __('Data Inicio') ?></th>
                <th scope="col"><?= __('Data Fim') ?></th>
                <th scope="col"><?= __('Motivo') ?></th>
                <th scope="col"><?= __('Recurso Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($recurso->indisponibilidade as $indisponibilidade): ?>
            <tr>
                <td><?= h($indisponibilidade->id) ?></td>
                <td><?= h($indisponibilidade->ativo) ?></td>
                <td><?= h($indisponibilidade->data_inicio) ?></td>
                <td><?= h($indisponibilidade->data_fim) ?></td>
                <td><?= h($indisponibilidade->motivo) ?></td>
                <td><?= h($indisponibilidade->recurso_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Indisponibilidade', 'action' => 'view', $indisponibilidade->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Indisponibilidade', 'action' => 'edit', $indisponibilidade->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Indisponibilidade', 'action' => 'delete', $indisponibilidade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $indisponibilidade->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Reserva') ?></h4>
        <?php if (!empty($recurso->reserva)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Apartamento Id') ?></th>
                <th scope="col"><?= __('Aprovada') ?></th>
                <th scope="col"><?= __('Cancelada') ?></th>
                <th scope="col"><?= __('Comentario') ?></th>
                <th scope="col"><?= __('Data Aprovacao') ?></th>
                <th scope="col"><?= __('Data Cancelamento') ?></th>
                <th scope="col"><?= __('Data Solicitacao') ?></th>
                <th scope="col"><?= __('Recurso Id') ?></th>
                <th scope="col"><?= __('Usuario Id') ?></th>
                <th scope="col"><?= __('Valor') ?></th>
                <th scope="col"><?= __('Data Fim Evento') ?></th>
                <th scope="col"><?= __('Data Inicio Evento') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($recurso->reserva as $reserva): ?>
            <tr>
                <td><?= h($reserva->id) ?></td>
                <td><?= h($reserva->apartamento_id) ?></td>
                <td><?= h($reserva->aprovada) ?></td>
                <td><?= h($reserva->cancelada) ?></td>
                <td><?= h($reserva->comentario) ?></td>
                <td><?= h($reserva->data_aprovacao) ?></td>
                <td><?= h($reserva->data_cancelamento) ?></td>
                <td><?= h($reserva->data_solicitacao) ?></td>
                <td><?= h($reserva->recurso_id) ?></td>
                <td><?= h($reserva->usuario_id) ?></td>
                <td><?= h($reserva->valor) ?></td>
                <td><?= h($reserva->data_fim_evento) ?></td>
                <td><?= h($reserva->data_inicio_evento) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Reserva', 'action' => 'view', $reserva->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Reserva', 'action' => 'edit', $reserva->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Reserva', 'action' => 'delete', $reserva->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reserva->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tipo Reserva') ?></h4>
        <?php if (!empty($recurso->tipo_reserva)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Descricao') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($recurso->tipo_reserva as $tipoReserva): ?>
            <tr>
                <td><?= h($tipoReserva->id) ?></td>
                <td><?= h($tipoReserva->descricao) ?></td>
                <td><?= h($tipoReserva->nome) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TipoReserva', 'action' => 'view', $tipoReserva->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TipoReserva', 'action' => 'edit', $tipoReserva->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TipoReserva', 'action' => 'delete', $tipoReserva->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tipoReserva->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
