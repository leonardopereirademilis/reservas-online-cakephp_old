<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $recurso->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $recurso->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Recurso'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Unidade Tempo Reserva'), ['controller' => 'UnidadeTempoReserva', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Unidade Tempo Reserva'), ['controller' => 'UnidadeTempoReserva', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cor'), ['controller' => 'Cor', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cor'), ['controller' => 'Cor', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Imagem'), ['controller' => 'Imagem', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Imagem'), ['controller' => 'Imagem', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Indisponibilidade'), ['controller' => 'Indisponibilidade', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Indisponibilidade'), ['controller' => 'Indisponibilidade', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Reserva'), ['controller' => 'Reserva', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Reserva'), ['controller' => 'Reserva', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipo Reserva'), ['controller' => 'TipoReserva', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipo Reserva'), ['controller' => 'TipoReserva', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="recurso form large-9 medium-8 columns content">
    <?= $this->Form->create($recurso) ?>
    <fieldset>
        <legend><?= __('Edit Recurso') ?></legend>
        <?php
            echo $this->Form->input('ativo');
            echo $this->Form->input('capacidade');
            echo $this->Form->input('condominio_id', ['options' => $condominio]);
            echo $this->Form->input('descricao');
            echo $this->Form->input('exige_confirmacao');
            echo $this->Form->input('nome');
            echo $this->Form->input('numero_max_reservas');
            echo $this->Form->input('tempo_reserva');
            echo $this->Form->input('valor');
            echo $this->Form->input('unidade_tempo_reserva_id', ['options' => $unidadeTempoReserva]);
            echo $this->Form->input('cor_id', ['options' => $cor]);
            echo $this->Form->input('tipo_reserva._ids', ['options' => $tipoReserva]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
