<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Recurso'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Unidade Tempo Reserva'), ['controller' => 'UnidadeTempoReserva', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Unidade Tempo Reserva'), ['controller' => 'UnidadeTempoReserva', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cor'), ['controller' => 'Cor', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cor'), ['controller' => 'Cor', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Imagem'), ['controller' => 'Imagem', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Imagem'), ['controller' => 'Imagem', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Indisponibilidade'), ['controller' => 'Indisponibilidade', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Indisponibilidade'), ['controller' => 'Indisponibilidade', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Reserva'), ['controller' => 'Reserva', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Reserva'), ['controller' => 'Reserva', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipo Reserva'), ['controller' => 'TipoReserva', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipo Reserva'), ['controller' => 'TipoReserva', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="recurso index large-9 medium-8 columns content">
    <h3><?= __('Recurso') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('capacidade') ?></th>
                <th scope="col"><?= $this->Paginator->sort('condominio_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nome') ?></th>
                <th scope="col"><?= $this->Paginator->sort('numero_max_reservas') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tempo_reserva') ?></th>
                <th scope="col"><?= $this->Paginator->sort('valor') ?></th>
                <th scope="col"><?= $this->Paginator->sort('unidade_tempo_reserva_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cor_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($recurso as $recurso): ?>
            <tr>
                <td><?= $this->Number->format($recurso->id) ?></td>
                <td><?= $this->Number->format($recurso->capacidade) ?></td>
                <td><?= $recurso->has('condominio') ? $this->Html->link($recurso->condominio->id, ['controller' => 'Condominio', 'action' => 'view', $recurso->condominio->id]) : '' ?></td>
                <td><?= h($recurso->nome) ?></td>
                <td><?= $this->Number->format($recurso->numero_max_reservas) ?></td>
                <td><?= $this->Number->format($recurso->tempo_reserva) ?></td>
                <td><?= $this->Number->format($recurso->valor) ?></td>
                <td><?= $recurso->has('unidade_tempo_reserva') ? $this->Html->link($recurso->unidade_tempo_reserva->id, ['controller' => 'UnidadeTempoReserva', 'action' => 'view', $recurso->unidade_tempo_reserva->id]) : '' ?></td>
                <td><?= $recurso->has('cor') ? $this->Html->link($recurso->cor->id, ['controller' => 'Cor', 'action' => 'view', $recurso->cor->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $recurso->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $recurso->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $recurso->id], ['confirm' => __('Are you sure you want to delete # {0}?', $recurso->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
