<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Unidade Tempo Reserva'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="unidadeTempoReserva index large-9 medium-8 columns content">
    <h3><?= __('Unidade Tempo Reserva') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nome') ?></th>
                <th scope="col"><?= $this->Paginator->sort('descricao') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($unidadeTempoReserva as $unidadeTempoReserva): ?>
            <tr>
                <td><?= $this->Number->format($unidadeTempoReserva->id) ?></td>
                <td><?= h($unidadeTempoReserva->nome) ?></td>
                <td><?= h($unidadeTempoReserva->descricao) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $unidadeTempoReserva->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $unidadeTempoReserva->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $unidadeTempoReserva->id], ['confirm' => __('Are you sure you want to delete # {0}?', $unidadeTempoReserva->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
