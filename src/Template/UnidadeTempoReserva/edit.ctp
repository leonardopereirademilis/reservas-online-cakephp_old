<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $unidadeTempoReserva->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $unidadeTempoReserva->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Unidade Tempo Reserva'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="unidadeTempoReserva form large-9 medium-8 columns content">
    <?= $this->Form->create($unidadeTempoReserva) ?>
    <fieldset>
        <legend><?= __('Edit Unidade Tempo Reserva') ?></legend>
        <?php
            echo $this->Form->input('nome');
            echo $this->Form->input('descricao');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
