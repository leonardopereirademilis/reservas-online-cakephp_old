<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Unidade Tempo Reserva'), ['action' => 'edit', $unidadeTempoReserva->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Unidade Tempo Reserva'), ['action' => 'delete', $unidadeTempoReserva->id], ['confirm' => __('Are you sure you want to delete # {0}?', $unidadeTempoReserva->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Unidade Tempo Reserva'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Unidade Tempo Reserva'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="unidadeTempoReserva view large-9 medium-8 columns content">
    <h3><?= h($unidadeTempoReserva->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($unidadeTempoReserva->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Descricao') ?></th>
            <td><?= h($unidadeTempoReserva->descricao) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($unidadeTempoReserva->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Recurso') ?></h4>
        <?php if (!empty($unidadeTempoReserva->recurso)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ativo') ?></th>
                <th scope="col"><?= __('Capacidade') ?></th>
                <th scope="col"><?= __('Condominio Id') ?></th>
                <th scope="col"><?= __('Descricao') ?></th>
                <th scope="col"><?= __('Exige Confirmacao') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Numero Max Reservas') ?></th>
                <th scope="col"><?= __('Tempo Reserva') ?></th>
                <th scope="col"><?= __('Valor') ?></th>
                <th scope="col"><?= __('Unidade Tempo Reserva Id') ?></th>
                <th scope="col"><?= __('Cor Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($unidadeTempoReserva->recurso as $recurso): ?>
            <tr>
                <td><?= h($recurso->id) ?></td>
                <td><?= h($recurso->ativo) ?></td>
                <td><?= h($recurso->capacidade) ?></td>
                <td><?= h($recurso->condominio_id) ?></td>
                <td><?= h($recurso->descricao) ?></td>
                <td><?= h($recurso->exige_confirmacao) ?></td>
                <td><?= h($recurso->nome) ?></td>
                <td><?= h($recurso->numero_max_reservas) ?></td>
                <td><?= h($recurso->tempo_reserva) ?></td>
                <td><?= h($recurso->valor) ?></td>
                <td><?= h($recurso->unidade_tempo_reserva_id) ?></td>
                <td><?= h($recurso->cor_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Recurso', 'action' => 'view', $recurso->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Recurso', 'action' => 'edit', $recurso->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Recurso', 'action' => 'delete', $recurso->id], ['confirm' => __('Are you sure you want to delete # {0}?', $recurso->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
