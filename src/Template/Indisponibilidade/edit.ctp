<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $indisponibilidade->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $indisponibilidade->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Indisponibilidade'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="indisponibilidade form large-9 medium-8 columns content">
    <?= $this->Form->create($indisponibilidade) ?>
    <fieldset>
        <legend><?= __('Edit Indisponibilidade') ?></legend>
        <?php
            echo $this->Form->input('ativo');
            echo $this->Form->input('data_inicio');
            echo $this->Form->input('data_fim');
            echo $this->Form->input('motivo');
            echo $this->Form->input('recurso_id', ['options' => $recurso]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
