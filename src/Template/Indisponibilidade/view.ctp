<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Indisponibilidade'), ['action' => 'edit', $indisponibilidade->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Indisponibilidade'), ['action' => 'delete', $indisponibilidade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $indisponibilidade->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Indisponibilidade'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Indisponibilidade'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="indisponibilidade view large-9 medium-8 columns content">
    <h3><?= h($indisponibilidade->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Motivo') ?></th>
            <td><?= h($indisponibilidade->motivo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Recurso') ?></th>
            <td><?= $indisponibilidade->has('recurso') ? $this->Html->link($indisponibilidade->recurso->id, ['controller' => 'Recurso', 'action' => 'view', $indisponibilidade->recurso->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($indisponibilidade->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Inicio') ?></th>
            <td><?= h($indisponibilidade->data_inicio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Fim') ?></th>
            <td><?= h($indisponibilidade->data_fim) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Ativo') ?></h4>
        <?= $this->Text->autoParagraph(h($indisponibilidade->ativo)); ?>
    </div>
</div>
