<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Indisponibilidade'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="indisponibilidade index large-9 medium-8 columns content">
    <h3><?= __('Indisponibilidade') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_inicio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_fim') ?></th>
                <th scope="col"><?= $this->Paginator->sort('motivo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('recurso_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($indisponibilidade as $indisponibilidade): ?>
            <tr>
                <td><?= $this->Number->format($indisponibilidade->id) ?></td>
                <td><?= h($indisponibilidade->data_inicio) ?></td>
                <td><?= h($indisponibilidade->data_fim) ?></td>
                <td><?= h($indisponibilidade->motivo) ?></td>
                <td><?= $indisponibilidade->has('recurso') ? $this->Html->link($indisponibilidade->recurso->id, ['controller' => 'Recurso', 'action' => 'view', $indisponibilidade->recurso->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $indisponibilidade->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $indisponibilidade->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $indisponibilidade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $indisponibilidade->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
