<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $convite->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $convite->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Convite'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Apartamento'), ['controller' => 'Apartamento', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Apartamento'), ['controller' => 'Apartamento', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="convite form large-9 medium-8 columns content">
    <?= $this->Form->create($convite) ?>
    <fieldset>
        <legend><?= __('Edit Convite') ?></legend>
        <?php
            echo $this->Form->input('apartamento_id', ['options' => $apartamento]);
            echo $this->Form->input('data_aceite', ['empty' => true]);
            echo $this->Form->input('data_convite');
            echo $this->Form->input('email');
            echo $this->Form->input('usuario_id', ['options' => $usuario, 'empty' => true]);
            echo $this->Form->input('usuario_solicitou');
            echo $this->Form->input('aprovado');
            echo $this->Form->input('condominio._ids', ['options' => $condominio]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
