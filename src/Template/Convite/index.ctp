<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Convite'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Apartamento'), ['controller' => 'Apartamento', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Apartamento'), ['controller' => 'Apartamento', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="convite index large-9 medium-8 columns content">
    <h3><?= __('Convite') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('apartamento_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_aceite') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_convite') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('usuario_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($convite as $convite): ?>
            <tr>
                <td><?= $this->Number->format($convite->id) ?></td>
                <td><?= $convite->has('apartamento') ? $this->Html->link($convite->apartamento->id, ['controller' => 'Apartamento', 'action' => 'view', $convite->apartamento->id]) : '' ?></td>
                <td><?= h($convite->data_aceite) ?></td>
                <td><?= h($convite->data_convite) ?></td>
                <td><?= h($convite->email) ?></td>
                <td><?= $convite->has('usuario') ? $this->Html->link($convite->usuario->id, ['controller' => 'Usuario', 'action' => 'view', $convite->usuario->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $convite->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $convite->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $convite->id], ['confirm' => __('Are you sure you want to delete # {0}?', $convite->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
