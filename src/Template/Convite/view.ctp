<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Convite'), ['action' => 'edit', $convite->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Convite'), ['action' => 'delete', $convite->id], ['confirm' => __('Are you sure you want to delete # {0}?', $convite->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Convite'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Convite'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Apartamento'), ['controller' => 'Apartamento', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Apartamento'), ['controller' => 'Apartamento', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="convite view large-9 medium-8 columns content">
    <h3><?= h($convite->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Apartamento') ?></th>
            <td><?= $convite->has('apartamento') ? $this->Html->link($convite->apartamento->id, ['controller' => 'Apartamento', 'action' => 'view', $convite->apartamento->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($convite->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usuario') ?></th>
            <td><?= $convite->has('usuario') ? $this->Html->link($convite->usuario->id, ['controller' => 'Usuario', 'action' => 'view', $convite->usuario->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($convite->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Aceite') ?></th>
            <td><?= h($convite->data_aceite) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Convite') ?></th>
            <td><?= h($convite->data_convite) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Usuario Solicitou') ?></h4>
        <?= $this->Text->autoParagraph(h($convite->usuario_solicitou)); ?>
    </div>
    <div class="row">
        <h4><?= __('Aprovado') ?></h4>
        <?= $this->Text->autoParagraph(h($convite->aprovado)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Condominio') ?></h4>
        <?php if (!empty($convite->condominio)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Endereco Id') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Plano Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($convite->condominio as $condominio): ?>
            <tr>
                <td><?= h($condominio->id) ?></td>
                <td><?= h($condominio->endereco_id) ?></td>
                <td><?= h($condominio->nome) ?></td>
                <td><?= h($condominio->plano_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Condominio', 'action' => 'view', $condominio->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Condominio', 'action' => 'edit', $condominio->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Condominio', 'action' => 'delete', $condominio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $condominio->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
