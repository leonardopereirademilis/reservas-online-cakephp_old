<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Endereco'), ['action' => 'edit', $endereco->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Endereco'), ['action' => 'delete', $endereco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $endereco->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Endereco'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Endereco'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cidade'), ['controller' => 'Cidade', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cidade'), ['controller' => 'Cidade', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="endereco view large-9 medium-8 columns content">
    <h3><?= h($endereco->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Bairro') ?></th>
            <td><?= h($endereco->bairro) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cep') ?></th>
            <td><?= h($endereco->cep) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cidade') ?></th>
            <td><?= $endereco->has('cidade') ? $this->Html->link($endereco->cidade->id, ['controller' => 'Cidade', 'action' => 'view', $endereco->cidade->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Complemento') ?></th>
            <td><?= h($endereco->complemento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Logradouro') ?></th>
            <td><?= h($endereco->logradouro) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($endereco->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Numero') ?></th>
            <td><?= $this->Number->format($endereco->numero) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Condominio') ?></h4>
        <?php if (!empty($endereco->condominio)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Endereco Id') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Plano Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($endereco->condominio as $condominio): ?>
            <tr>
                <td><?= h($condominio->id) ?></td>
                <td><?= h($condominio->endereco_id) ?></td>
                <td><?= h($condominio->nome) ?></td>
                <td><?= h($condominio->plano_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Condominio', 'action' => 'view', $condominio->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Condominio', 'action' => 'edit', $condominio->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Condominio', 'action' => 'delete', $condominio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $condominio->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
