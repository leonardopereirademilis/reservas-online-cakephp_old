<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Reserva'), ['action' => 'edit', $reserva->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Reserva'), ['action' => 'delete', $reserva->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reserva->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Reserva'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Reserva'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Apartamento'), ['controller' => 'Apartamento', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Apartamento'), ['controller' => 'Apartamento', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Convidado'), ['controller' => 'Convidado', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Convidado'), ['controller' => 'Convidado', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="reserva view large-9 medium-8 columns content">
    <h3><?= h($reserva->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Apartamento') ?></th>
            <td><?= $reserva->has('apartamento') ? $this->Html->link($reserva->apartamento->id, ['controller' => 'Apartamento', 'action' => 'view', $reserva->apartamento->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Comentario') ?></th>
            <td><?= h($reserva->comentario) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Recurso') ?></th>
            <td><?= $reserva->has('recurso') ? $this->Html->link($reserva->recurso->id, ['controller' => 'Recurso', 'action' => 'view', $reserva->recurso->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usuario') ?></th>
            <td><?= $reserva->has('usuario') ? $this->Html->link($reserva->usuario->id, ['controller' => 'Usuario', 'action' => 'view', $reserva->usuario->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($reserva->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Valor') ?></th>
            <td><?= $this->Number->format($reserva->valor) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Aprovacao') ?></th>
            <td><?= h($reserva->data_aprovacao) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Cancelamento') ?></th>
            <td><?= h($reserva->data_cancelamento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Solicitacao') ?></th>
            <td><?= h($reserva->data_solicitacao) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Fim Evento') ?></th>
            <td><?= h($reserva->data_fim_evento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Inicio Evento') ?></th>
            <td><?= h($reserva->data_inicio_evento) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Aprovada') ?></h4>
        <?= $this->Text->autoParagraph(h($reserva->aprovada)); ?>
    </div>
    <div class="row">
        <h4><?= __('Cancelada') ?></h4>
        <?= $this->Text->autoParagraph(h($reserva->cancelada)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Convidado') ?></h4>
        <?php if (!empty($reserva->convidado)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Cpf') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Reserva Id') ?></th>
                <th scope="col"><?= __('Telefone') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($reserva->convidado as $convidado): ?>
            <tr>
                <td><?= h($convidado->id) ?></td>
                <td><?= h($convidado->cpf) ?></td>
                <td><?= h($convidado->email) ?></td>
                <td><?= h($convidado->nome) ?></td>
                <td><?= h($convidado->reserva_id) ?></td>
                <td><?= h($convidado->telefone) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Convidado', 'action' => 'view', $convidado->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Convidado', 'action' => 'edit', $convidado->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Convidado', 'action' => 'delete', $convidado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $convidado->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
