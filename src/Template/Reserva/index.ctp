<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Reserva'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Apartamento'), ['controller' => 'Apartamento', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Apartamento'), ['controller' => 'Apartamento', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Convidado'), ['controller' => 'Convidado', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Convidado'), ['controller' => 'Convidado', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="reserva index large-9 medium-8 columns content">
    <h3><?= __('Reserva') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('apartamento_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('comentario') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_aprovacao') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_cancelamento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_solicitacao') ?></th>
                <th scope="col"><?= $this->Paginator->sort('recurso_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('usuario_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('valor') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_fim_evento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_inicio_evento') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($reserva as $reserva): ?>
            <tr>
                <td><?= $this->Number->format($reserva->id) ?></td>
                <td><?= $reserva->has('apartamento') ? $this->Html->link($reserva->apartamento->id, ['controller' => 'Apartamento', 'action' => 'view', $reserva->apartamento->id]) : '' ?></td>
                <td><?= h($reserva->comentario) ?></td>
                <td><?= h($reserva->data_aprovacao) ?></td>
                <td><?= h($reserva->data_cancelamento) ?></td>
                <td><?= h($reserva->data_solicitacao) ?></td>
                <td><?= $reserva->has('recurso') ? $this->Html->link($reserva->recurso->id, ['controller' => 'Recurso', 'action' => 'view', $reserva->recurso->id]) : '' ?></td>
                <td><?= $reserva->has('usuario') ? $this->Html->link($reserva->usuario->id, ['controller' => 'Usuario', 'action' => 'view', $reserva->usuario->id]) : '' ?></td>
                <td><?= $this->Number->format($reserva->valor) ?></td>
                <td><?= h($reserva->data_fim_evento) ?></td>
                <td><?= h($reserva->data_inicio_evento) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $reserva->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $reserva->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $reserva->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reserva->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
