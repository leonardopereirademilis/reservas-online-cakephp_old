<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Reserva'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Apartamento'), ['controller' => 'Apartamento', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Apartamento'), ['controller' => 'Apartamento', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Convidado'), ['controller' => 'Convidado', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Convidado'), ['controller' => 'Convidado', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="reserva form large-9 medium-8 columns content">
    <?= $this->Form->create($reserva) ?>
    <fieldset>
        <legend><?= __('Add Reserva') ?></legend>
        <?php
            echo $this->Form->input('apartamento_id', ['options' => $apartamento]);
            echo $this->Form->input('aprovada');
            echo $this->Form->input('cancelada');
            echo $this->Form->input('comentario');
            echo $this->Form->input('data_aprovacao', ['empty' => true]);
            echo $this->Form->input('data_cancelamento', ['empty' => true]);
            echo $this->Form->input('data_solicitacao');
            echo $this->Form->input('recurso_id', ['options' => $recurso]);
            echo $this->Form->input('usuario_id', ['options' => $usuario]);
            echo $this->Form->input('valor');
            echo $this->Form->input('data_fim_evento');
            echo $this->Form->input('data_inicio_evento');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
