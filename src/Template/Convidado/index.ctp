<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Convidado'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Reserva'), ['controller' => 'Reserva', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Reserva'), ['controller' => 'Reserva', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="convidado index large-9 medium-8 columns content">
    <h3><?= __('Convidado') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cpf') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nome') ?></th>
                <th scope="col"><?= $this->Paginator->sort('reserva_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('telefone') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($convidado as $convidado): ?>
            <tr>
                <td><?= $this->Number->format($convidado->id) ?></td>
                <td><?= h($convidado->cpf) ?></td>
                <td><?= h($convidado->email) ?></td>
                <td><?= h($convidado->nome) ?></td>
                <td><?= $convidado->has('reserva') ? $this->Html->link($convidado->reserva->id, ['controller' => 'Reserva', 'action' => 'view', $convidado->reserva->id]) : '' ?></td>
                <td><?= h($convidado->telefone) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $convidado->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $convidado->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $convidado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $convidado->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
