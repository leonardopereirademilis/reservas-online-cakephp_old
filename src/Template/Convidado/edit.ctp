<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $convidado->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $convidado->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Convidado'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Reserva'), ['controller' => 'Reserva', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Reserva'), ['controller' => 'Reserva', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="convidado form large-9 medium-8 columns content">
    <?= $this->Form->create($convidado) ?>
    <fieldset>
        <legend><?= __('Edit Convidado') ?></legend>
        <?php
            echo $this->Form->input('cpf');
            echo $this->Form->input('email');
            echo $this->Form->input('nome');
            echo $this->Form->input('reserva_id', ['options' => $reserva]);
            echo $this->Form->input('telefone');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
