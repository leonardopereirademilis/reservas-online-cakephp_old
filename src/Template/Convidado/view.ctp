<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Convidado'), ['action' => 'edit', $convidado->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Convidado'), ['action' => 'delete', $convidado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $convidado->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Convidado'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Convidado'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Reserva'), ['controller' => 'Reserva', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Reserva'), ['controller' => 'Reserva', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="convidado view large-9 medium-8 columns content">
    <h3><?= h($convidado->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Cpf') ?></th>
            <td><?= h($convidado->cpf) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($convidado->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($convidado->nome) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Reserva') ?></th>
            <td><?= $convidado->has('reserva') ? $this->Html->link($convidado->reserva->id, ['controller' => 'Reserva', 'action' => 'view', $convidado->reserva->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Telefone') ?></th>
            <td><?= h($convidado->telefone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($convidado->id) ?></td>
        </tr>
    </table>
</div>
