<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Plano'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tipo Plano'), ['controller' => 'TipoPlano', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipo Plano'), ['controller' => 'TipoPlano', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="plano form large-9 medium-8 columns content">
    <?= $this->Form->create($plano) ?>
    <fieldset>
        <legend><?= __('Add Plano') ?></legend>
        <?php
            echo $this->Form->input('ativo');
            echo $this->Form->input('data_fim', ['empty' => true]);
            echo $this->Form->input('data_inicio');
            echo $this->Form->input('tipo_plano_id', ['options' => $tipoPlano]);
            echo $this->Form->input('usuario_id', ['options' => $usuario]);
            echo $this->Form->input('planos_idx');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
