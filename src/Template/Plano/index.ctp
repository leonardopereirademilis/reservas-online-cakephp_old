<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Plano'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tipo Plano'), ['controller' => 'TipoPlano', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tipo Plano'), ['controller' => 'TipoPlano', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="plano index large-9 medium-8 columns content">
    <h3><?= __('Plano') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_fim') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_inicio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tipo_plano_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('usuario_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('planos_idx') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($plano as $plano): ?>
            <tr>
                <td><?= $this->Number->format($plano->id) ?></td>
                <td><?= h($plano->data_fim) ?></td>
                <td><?= h($plano->data_inicio) ?></td>
                <td><?= $plano->has('tipo_plano') ? $this->Html->link($plano->tipo_plano->id, ['controller' => 'TipoPlano', 'action' => 'view', $plano->tipo_plano->id]) : '' ?></td>
                <td><?= $plano->has('usuario') ? $this->Html->link($plano->usuario->id, ['controller' => 'Usuario', 'action' => 'view', $plano->usuario->id]) : '' ?></td>
                <td><?= $this->Number->format($plano->planos_idx) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $plano->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $plano->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $plano->id], ['confirm' => __('Are you sure you want to delete # {0}?', $plano->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
