<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Plano'), ['action' => 'edit', $plano->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Plano'), ['action' => 'delete', $plano->id], ['confirm' => __('Are you sure you want to delete # {0}?', $plano->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Plano'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Plano'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tipo Plano'), ['controller' => 'TipoPlano', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tipo Plano'), ['controller' => 'TipoPlano', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Usuario'), ['controller' => 'Usuario', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuario', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mensalidade'), ['controller' => 'Mensalidade', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="plano view large-9 medium-8 columns content">
    <h3><?= h($plano->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Tipo Plano') ?></th>
            <td><?= $plano->has('tipo_plano') ? $this->Html->link($plano->tipo_plano->id, ['controller' => 'TipoPlano', 'action' => 'view', $plano->tipo_plano->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usuario') ?></th>
            <td><?= $plano->has('usuario') ? $this->Html->link($plano->usuario->id, ['controller' => 'Usuario', 'action' => 'view', $plano->usuario->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($plano->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Planos Idx') ?></th>
            <td><?= $this->Number->format($plano->planos_idx) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Fim') ?></th>
            <td><?= h($plano->data_fim) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Inicio') ?></th>
            <td><?= h($plano->data_inicio) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Ativo') ?></h4>
        <?= $this->Text->autoParagraph(h($plano->ativo)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Condominio') ?></h4>
        <?php if (!empty($plano->condominio)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Endereco Id') ?></th>
                <th scope="col"><?= __('Nome') ?></th>
                <th scope="col"><?= __('Plano Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($plano->condominio as $condominio): ?>
            <tr>
                <td><?= h($condominio->id) ?></td>
                <td><?= h($condominio->endereco_id) ?></td>
                <td><?= h($condominio->nome) ?></td>
                <td><?= h($condominio->plano_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Condominio', 'action' => 'view', $condominio->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Condominio', 'action' => 'edit', $condominio->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Condominio', 'action' => 'delete', $condominio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $condominio->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Mensalidade') ?></h4>
        <?php if (!empty($plano->mensalidade)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ano') ?></th>
                <th scope="col"><?= __('Condominio Id') ?></th>
                <th scope="col"><?= __('Mes') ?></th>
                <th scope="col"><?= __('Plano Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($plano->mensalidade as $mensalidade): ?>
            <tr>
                <td><?= h($mensalidade->id) ?></td>
                <td><?= h($mensalidade->ano) ?></td>
                <td><?= h($mensalidade->condominio_id) ?></td>
                <td><?= h($mensalidade->mes) ?></td>
                <td><?= h($mensalidade->plano_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Mensalidade', 'action' => 'view', $mensalidade->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Mensalidade', 'action' => 'edit', $mensalidade->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Mensalidade', 'action' => 'delete', $mensalidade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mensalidade->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
