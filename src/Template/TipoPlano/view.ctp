<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tipo Plano'), ['action' => 'edit', $tipoPlano->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tipo Plano'), ['action' => 'delete', $tipoPlano->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tipoPlano->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tipo Plano'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tipo Plano'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Plano'), ['controller' => 'Plano', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Plano'), ['controller' => 'Plano', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tipoPlano view large-9 medium-8 columns content">
    <h3><?= h($tipoPlano->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($tipoPlano->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nu Max Apartamentos') ?></th>
            <td><?= $this->Number->format($tipoPlano->nu_max_apartamentos) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nu Max Recursos') ?></th>
            <td><?= $this->Number->format($tipoPlano->nu_max_recursos) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Valor') ?></th>
            <td><?= $this->Number->format($tipoPlano->valor) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Criacao') ?></th>
            <td><?= h($tipoPlano->data_criacao) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data Encerramento') ?></th>
            <td><?= h($tipoPlano->data_encerramento) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Ativo') ?></h4>
        <?= $this->Text->autoParagraph(h($tipoPlano->ativo)); ?>
    </div>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($tipoPlano->descricao)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Plano') ?></h4>
        <?php if (!empty($tipoPlano->plano)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ativo') ?></th>
                <th scope="col"><?= __('Data Fim') ?></th>
                <th scope="col"><?= __('Data Inicio') ?></th>
                <th scope="col"><?= __('Tipo Plano Id') ?></th>
                <th scope="col"><?= __('Usuario Id') ?></th>
                <th scope="col"><?= __('Planos Idx') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tipoPlano->plano as $plano): ?>
            <tr>
                <td><?= h($plano->id) ?></td>
                <td><?= h($plano->ativo) ?></td>
                <td><?= h($plano->data_fim) ?></td>
                <td><?= h($plano->data_inicio) ?></td>
                <td><?= h($plano->tipo_plano_id) ?></td>
                <td><?= h($plano->usuario_id) ?></td>
                <td><?= h($plano->planos_idx) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Plano', 'action' => 'view', $plano->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Plano', 'action' => 'edit', $plano->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Plano', 'action' => 'delete', $plano->id], ['confirm' => __('Are you sure you want to delete # {0}?', $plano->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
