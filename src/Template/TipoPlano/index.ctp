<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Tipo Plano'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Plano'), ['controller' => 'Plano', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Plano'), ['controller' => 'Plano', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tipoPlano index large-9 medium-8 columns content">
    <h3><?= __('Tipo Plano') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_criacao') ?></th>
                <th scope="col"><?= $this->Paginator->sort('data_encerramento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nu_max_apartamentos') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nu_max_recursos') ?></th>
                <th scope="col"><?= $this->Paginator->sort('valor') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tipoPlano as $tipoPlano): ?>
            <tr>
                <td><?= $this->Number->format($tipoPlano->id) ?></td>
                <td><?= h($tipoPlano->data_criacao) ?></td>
                <td><?= h($tipoPlano->data_encerramento) ?></td>
                <td><?= $this->Number->format($tipoPlano->nu_max_apartamentos) ?></td>
                <td><?= $this->Number->format($tipoPlano->nu_max_recursos) ?></td>
                <td><?= $this->Number->format($tipoPlano->valor) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $tipoPlano->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tipoPlano->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $tipoPlano->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tipoPlano->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
