<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Tipo Plano'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Plano'), ['controller' => 'Plano', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Plano'), ['controller' => 'Plano', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tipoPlano form large-9 medium-8 columns content">
    <?= $this->Form->create($tipoPlano) ?>
    <fieldset>
        <legend><?= __('Add Tipo Plano') ?></legend>
        <?php
            echo $this->Form->input('ativo');
            echo $this->Form->input('data_criacao');
            echo $this->Form->input('data_encerramento', ['empty' => true]);
            echo $this->Form->input('nu_max_apartamentos');
            echo $this->Form->input('nu_max_recursos');
            echo $this->Form->input('valor');
            echo $this->Form->input('descricao');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
