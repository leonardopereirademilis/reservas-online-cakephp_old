<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $tipoReserva->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $tipoReserva->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Tipo Reserva'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Recurso'), ['controller' => 'Recurso', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recurso', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tipoReserva form large-9 medium-8 columns content">
    <?= $this->Form->create($tipoReserva) ?>
    <fieldset>
        <legend><?= __('Edit Tipo Reserva') ?></legend>
        <?php
            echo $this->Form->input('descricao');
            echo $this->Form->input('nome');
            echo $this->Form->input('recurso._ids', ['options' => $recurso]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
