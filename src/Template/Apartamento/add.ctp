<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Apartamento'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Convite'), ['controller' => 'Convite', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Convite'), ['controller' => 'Convite', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Reserva'), ['controller' => 'Reserva', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Reserva'), ['controller' => 'Reserva', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="apartamento form large-9 medium-8 columns content">
    <?= $this->Form->create($apartamento) ?>
    <fieldset>
        <legend><?= __('Add Apartamento') ?></legend>
        <?php
            echo $this->Form->input('bloco');
            echo $this->Form->input('condominio_id', ['options' => $condominio]);
            echo $this->Form->input('numero');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
