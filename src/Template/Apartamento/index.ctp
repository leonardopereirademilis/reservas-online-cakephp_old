<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Apartamento'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Convite'), ['controller' => 'Convite', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Convite'), ['controller' => 'Convite', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Reserva'), ['controller' => 'Reserva', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Reserva'), ['controller' => 'Reserva', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="apartamento index large-9 medium-8 columns content">
    <h3><?= __('Apartamento') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('bloco') ?></th>
                <th scope="col"><?= $this->Paginator->sort('condominio_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('numero') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($apartamento as $apartamento): ?>
            <tr>
                <td><?= $this->Number->format($apartamento->id) ?></td>
                <td><?= h($apartamento->bloco) ?></td>
                <td><?= $apartamento->has('condominio') ? $this->Html->link($apartamento->condominio->id, ['controller' => 'Condominio', 'action' => 'view', $apartamento->condominio->id]) : '' ?></td>
                <td><?= $this->Number->format($apartamento->numero) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $apartamento->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $apartamento->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $apartamento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $apartamento->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
