<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('../Menu/menu'); ?><ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Apartamento'), ['action' => 'edit', $apartamento->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Apartamento'), ['action' => 'delete', $apartamento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $apartamento->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Apartamento'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Apartamento'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Condominio'), ['controller' => 'Condominio', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Condominio'), ['controller' => 'Condominio', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Convite'), ['controller' => 'Convite', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Convite'), ['controller' => 'Convite', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Reserva'), ['controller' => 'Reserva', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Reserva'), ['controller' => 'Reserva', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="apartamento view large-9 medium-8 columns content">
    <h3><?= h($apartamento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Bloco') ?></th>
            <td><?= h($apartamento->bloco) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Condominio') ?></th>
            <td><?= $apartamento->has('condominio') ? $this->Html->link($apartamento->condominio->id, ['controller' => 'Condominio', 'action' => 'view', $apartamento->condominio->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($apartamento->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Numero') ?></th>
            <td><?= $this->Number->format($apartamento->numero) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Convite') ?></h4>
        <?php if (!empty($apartamento->convite)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Apartamento Id') ?></th>
                <th scope="col"><?= __('Data Aceite') ?></th>
                <th scope="col"><?= __('Data Convite') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Usuario Id') ?></th>
                <th scope="col"><?= __('Usuario Solicitou') ?></th>
                <th scope="col"><?= __('Aprovado') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($apartamento->convite as $convite): ?>
            <tr>
                <td><?= h($convite->id) ?></td>
                <td><?= h($convite->apartamento_id) ?></td>
                <td><?= h($convite->data_aceite) ?></td>
                <td><?= h($convite->data_convite) ?></td>
                <td><?= h($convite->email) ?></td>
                <td><?= h($convite->usuario_id) ?></td>
                <td><?= h($convite->usuario_solicitou) ?></td>
                <td><?= h($convite->aprovado) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Convite', 'action' => 'view', $convite->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Convite', 'action' => 'edit', $convite->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Convite', 'action' => 'delete', $convite->id], ['confirm' => __('Are you sure you want to delete # {0}?', $convite->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Reserva') ?></h4>
        <?php if (!empty($apartamento->reserva)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Apartamento Id') ?></th>
                <th scope="col"><?= __('Aprovada') ?></th>
                <th scope="col"><?= __('Cancelada') ?></th>
                <th scope="col"><?= __('Comentario') ?></th>
                <th scope="col"><?= __('Data Aprovacao') ?></th>
                <th scope="col"><?= __('Data Cancelamento') ?></th>
                <th scope="col"><?= __('Data Solicitacao') ?></th>
                <th scope="col"><?= __('Recurso Id') ?></th>
                <th scope="col"><?= __('Usuario Id') ?></th>
                <th scope="col"><?= __('Valor') ?></th>
                <th scope="col"><?= __('Data Fim Evento') ?></th>
                <th scope="col"><?= __('Data Inicio Evento') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($apartamento->reserva as $reserva): ?>
            <tr>
                <td><?= h($reserva->id) ?></td>
                <td><?= h($reserva->apartamento_id) ?></td>
                <td><?= h($reserva->aprovada) ?></td>
                <td><?= h($reserva->cancelada) ?></td>
                <td><?= h($reserva->comentario) ?></td>
                <td><?= h($reserva->data_aprovacao) ?></td>
                <td><?= h($reserva->data_cancelamento) ?></td>
                <td><?= h($reserva->data_solicitacao) ?></td>
                <td><?= h($reserva->recurso_id) ?></td>
                <td><?= h($reserva->usuario_id) ?></td>
                <td><?= h($reserva->valor) ?></td>
                <td><?= h($reserva->data_fim_evento) ?></td>
                <td><?= h($reserva->data_inicio_evento) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Reserva', 'action' => 'view', $reserva->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Reserva', 'action' => 'edit', $reserva->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Reserva', 'action' => 'delete', $reserva->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reserva->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
