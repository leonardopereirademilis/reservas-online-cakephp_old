    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        var index = 0;
        <?php
            $controllers = getFrontControllers();

            foreach($controllers as $index => $controller){
                $controller = str_replace("Controller","",$controller);
                if($controller == $this->name){
                    echo "index = $index;";
                }
            }
        ?>

        $( function() {
            $( "#accordion-menu" ).accordion({
                heightStyle: "content",
                active: index
            });
        } );
    </script>

    <?php

    function getFrontControllers()
    {
        $files = scandir('../src/Controller/');
        $results = [];
        $ignoreList = [
            '.',
            '..',
            'Component',
            'Admin',
            'PagesController.php',
            'AppController.php',
        ];
        foreach($files as $file){
            if(!in_array($file, $ignoreList)) {
                $controller = explode('.', $file)[0];
                //array_push($results, str_replace('Controller', '', $controller));
                array_push($results, $controller);
            }
        }
        return $results;
    }

    function getAdminControllers()
    {
        $files = scandir('../src/Controller/Admin/');
        $results = [];
        $ignoreList = [
            '.',
            '..',
            'PagesController.php',
        ];
        foreach($files as $file){
            if(!in_array($file, $ignoreList)) {
                $controller = explode('.', $file)[0];
                //array_push($results, str_replace('Controller', '', $controller));
                array_push($results, $controller);
            }
        }
        return $results;
    }

    function getAdminActions($controllerName)
    {
        $className = 'App\\Controller\\Admin\\'.$controllerName;
        $class = new ReflectionClass($className);
        $actions = $class->getMethods(ReflectionMethod::IS_PUBLIC);

        $results = [$controllerName => []];
        $ignoreList = ['beforeFilter', 'afterFilter', 'initialize'];
        foreach($actions as $action){
            if($action->class == $className && !in_array($action->name, $ignoreList)){
                array_push($results[$controllerName], $action->name);
            }
        }
        return $results;
    }

    function getFrontActions($controllerName)
    {
        $className = 'App\\Controller\\'.$controllerName;
        $class = new ReflectionClass($className);
        $actions = $class->getMethods(ReflectionMethod::IS_PUBLIC);

        $results = [$controllerName => []];
        $ignoreList = ['beforeFilter', 'afterFilter', 'initialize'];
        foreach($actions as $action){
            if($action->class == $className && !in_array($action->name, $ignoreList)){
                array_push($results[$controllerName], $action->name);
            }
        }
        return $results;
    }


    //Return Front And Admin Controller => actions list
    function getResources()
    {
        $Front_controllers = $this->getFrontControllers();

        $resources['Front'] = [];
        foreach($Front_controllers as $controller)
        {
            $Front_actions = $this->getFrontActions($controller);
            //Empty controller Ignore
            if(!empty($Front_actions[$controller])){
                $resources['Front'] = array_merge($resources['Front'], $Front_actions);
            }
        }

        $Admin_controllers = $this->getAdminControllers();
        $resources['Admin'] = [];
        foreach($Admin_controllers as $controller)
        {
            $Admin_actions = $this->getAdminActions($controller);
            //Empty controller Ignore
            if(!empty($Admin_actions[$controller])){
                $resources['Admin'] = array_merge($resources['Admin'], $Admin_actions);
            }
        }
        return $resources;
    }

    echo '<div id="accordion-menu">';
    foreach($controllers as $controller){
        $controller_str = str_replace('Controller', '', $controller);
        echo '<h3><i class="glyphicon glyphicon-'.strtolower($controller_str).'"></i><span class="menu-title">'.$controller_str.'</span></h3>';
        echo '<div>';

//        echo '<li class="heading">' . __('Actions') . '</li>';

        $actions = getFrontActions($controller);
        $ignoreList = ['view', 'edit', 'delete'];
        foreach($actions[$controller] as $action){
            if(!in_array($action,$ignoreList)) {
                $action_name = $action;
                $action_name = str_replace('index', 'Listar', $action_name);
                $action_name = str_replace('add', 'Adicionar', $action_name);
                echo '<div>';
                echo '<li>' . $this->Html->link(__($action_name . ' ' . $controller_str), ['controller' => $controller_str, 'action' => $action]) . '</li>';
                echo '</div>';
            }
        }

        echo '</div>';
    }
    echo "</div>";

    ?>