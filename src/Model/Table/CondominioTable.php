<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Condominio Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Endereco
 * @property \Cake\ORM\Association\BelongsTo $Plano
 * @property \Cake\ORM\Association\HasMany $Apartamento
 * @property \Cake\ORM\Association\HasMany $Mensalidade
 * @property \Cake\ORM\Association\HasMany $Recurso
 * @property \Cake\ORM\Association\BelongsToMany $Convite
 * @property \Cake\ORM\Association\BelongsToMany $Usuario
 *
 * @method \App\Model\Entity\Condominio get($primaryKey, $options = [])
 * @method \App\Model\Entity\Condominio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Condominio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Condominio|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Condominio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Condominio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Condominio findOrCreate($search, callable $callback = null)
 */
class CondominioTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('condominio');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Endereco', [
            'foreignKey' => 'endereco_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Plano', [
            'foreignKey' => 'plano_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Apartamento', [
            'foreignKey' => 'condominio_id'
        ]);
        $this->hasMany('Mensalidade', [
            'foreignKey' => 'condominio_id'
        ]);
        $this->hasMany('Recurso', [
            'foreignKey' => 'condominio_id'
        ]);
        $this->belongsToMany('Convite', [
            'foreignKey' => 'condominio_id',
            'targetForeignKey' => 'convite_id',
            'joinTable' => 'condominio_convite'
        ]);
        $this->belongsToMany('Usuario', [
            'foreignKey' => 'condominio_id',
            'targetForeignKey' => 'usuario_id',
            'joinTable' => 'condominio_usuario'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['plano_id'], 'Plano'));
        $rules->add($rules->existsIn(['endereco_id'], 'Endereco'));

        return $rules;
    }
}
