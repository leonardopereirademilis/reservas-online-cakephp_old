<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Papel Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Usuario
 *
 * @method \App\Model\Entity\Papel get($primaryKey, $options = [])
 * @method \App\Model\Entity\Papel newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Papel[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Papel|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Papel patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Papel[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Papel findOrCreate($search, callable $callback = null)
 */
class PapelTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('papel');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Usuario', [
            'foreignKey' => 'papel_id',
            'targetForeignKey' => 'usuario_id',
            'joinTable' => 'usuario_papel'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('authority', 'create')
            ->notEmpty('authority')
            ->add('authority', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['authority']));

        return $rules;
    }
}
