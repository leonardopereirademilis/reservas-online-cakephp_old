<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Convite Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Apartamento
 * @property \Cake\ORM\Association\BelongsTo $Usuario
 * @property \Cake\ORM\Association\BelongsToMany $Condominio
 *
 * @method \App\Model\Entity\Convite get($primaryKey, $options = [])
 * @method \App\Model\Entity\Convite newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Convite[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Convite|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Convite patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Convite[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Convite findOrCreate($search, callable $callback = null)
 */
class ConviteTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('convite');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Apartamento', [
            'foreignKey' => 'apartamento_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Usuario', [
            'foreignKey' => 'usuario_id'
        ]);
        $this->belongsToMany('Condominio', [
            'foreignKey' => 'convite_id',
            'targetForeignKey' => 'condominio_id',
            'joinTable' => 'condominio_convite'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('data_aceite')
            ->allowEmpty('data_aceite');

        $validator
            ->dateTime('data_convite')
            ->requirePresence('data_convite', 'create')
            ->notEmpty('data_convite');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->requirePresence('usuario_solicitou', 'create')
            ->notEmpty('usuario_solicitou');

        $validator
            ->allowEmpty('aprovado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['apartamento_id'], 'Apartamento'));
        $rules->add($rules->existsIn(['usuario_id'], 'Usuario'));

        return $rules;
    }
}
