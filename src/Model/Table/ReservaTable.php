<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Reserva Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Apartamento
 * @property \Cake\ORM\Association\BelongsTo $Recurso
 * @property \Cake\ORM\Association\BelongsTo $Usuario
 * @property \Cake\ORM\Association\HasMany $Convidado
 *
 * @method \App\Model\Entity\Reserva get($primaryKey, $options = [])
 * @method \App\Model\Entity\Reserva newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Reserva[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Reserva|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Reserva patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Reserva[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Reserva findOrCreate($search, callable $callback = null)
 */
class ReservaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('reserva');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Apartamento', [
            'foreignKey' => 'apartamento_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Recurso', [
            'foreignKey' => 'recurso_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Usuario', [
            'foreignKey' => 'usuario_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Convidado', [
            'foreignKey' => 'reserva_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('aprovada', 'create')
            ->notEmpty('aprovada');

        $validator
            ->requirePresence('cancelada', 'create')
            ->notEmpty('cancelada');

        $validator
            ->requirePresence('comentario', 'create')
            ->notEmpty('comentario');

        $validator
            ->dateTime('data_aprovacao')
            ->allowEmpty('data_aprovacao');

        $validator
            ->dateTime('data_cancelamento')
            ->allowEmpty('data_cancelamento');

        $validator
            ->dateTime('data_solicitacao')
            ->requirePresence('data_solicitacao', 'create')
            ->notEmpty('data_solicitacao');

        $validator
            ->decimal('valor')
            ->requirePresence('valor', 'create')
            ->notEmpty('valor');

        $validator
            ->dateTime('data_fim_evento')
            ->requirePresence('data_fim_evento', 'create')
            ->notEmpty('data_fim_evento');

        $validator
            ->dateTime('data_inicio_evento')
            ->requirePresence('data_inicio_evento', 'create')
            ->notEmpty('data_inicio_evento');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['apartamento_id'], 'Apartamento'));
        $rules->add($rules->existsIn(['recurso_id'], 'Recurso'));
        $rules->add($rules->existsIn(['usuario_id'], 'Usuario'));

        return $rules;
    }
}
