<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UsuarioPapel Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Papel
 * @property \Cake\ORM\Association\BelongsTo $Usuario
 *
 * @method \App\Model\Entity\UsuarioPapel get($primaryKey, $options = [])
 * @method \App\Model\Entity\UsuarioPapel newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UsuarioPapel[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UsuarioPapel|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsuarioPapel patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UsuarioPapel[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UsuarioPapel findOrCreate($search, callable $callback = null)
 */
class UsuarioPapelTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('usuario_papel');
        $this->displayField('papel_id');
        $this->primaryKey(['papel_id', 'usuario_id']);

        $this->belongsTo('Papel', [
            'foreignKey' => 'papel_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Usuario', [
            'foreignKey' => 'usuario_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['papel_id'], 'Papel'));
        $rules->add($rules->existsIn(['usuario_id'], 'Usuario'));

        return $rules;
    }
}
