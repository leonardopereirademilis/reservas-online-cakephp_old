<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Usuario Model
 *
 * @property \Cake\ORM\Association\HasMany $Convite
 * @property \Cake\ORM\Association\HasMany $Plano
 * @property \Cake\ORM\Association\HasMany $Reserva
 * @property \Cake\ORM\Association\BelongsToMany $Condominio
 * @property \Cake\ORM\Association\BelongsToMany $Papel
 *
 * @method \App\Model\Entity\Usuario get($primaryKey, $options = [])
 * @method \App\Model\Entity\Usuario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Usuario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Usuario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usuario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Usuario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Usuario findOrCreate($search, callable $callback = null)
 */
class UsuarioTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('usuario');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Convite', [
            'foreignKey' => 'usuario_id'
        ]);
        $this->hasMany('Plano', [
            'foreignKey' => 'usuario_id'
        ]);
        $this->hasMany('Reserva', [
            'foreignKey' => 'usuario_id'
        ]);
        $this->belongsToMany('Condominio', [
            'foreignKey' => 'usuario_id',
            'targetForeignKey' => 'condominio_id',
            'joinTable' => 'condominio_usuario'
        ]);
        $this->belongsToMany('Papel', [
            'foreignKey' => 'usuario_id',
            'targetForeignKey' => 'papel_id',
            'joinTable' => 'usuario_papel'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('account_expired', 'create')
            ->notEmpty('account_expired');

        $validator
            ->requirePresence('account_locked', 'create')
            ->notEmpty('account_locked');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('enabled', 'create')
            ->notEmpty('enabled');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->requirePresence('password_expired', 'create')
            ->notEmpty('password_expired');

        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username')
            ->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['username']));

        return $rules;
    }
}
