<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Condominio Entity
 *
 * @property int $id
 * @property int $endereco_id
 * @property string $nome
 * @property int $plano_id
 *
 * @property \App\Model\Entity\Endereco $endereco
 * @property \App\Model\Entity\Plano $plano
 * @property \App\Model\Entity\Apartamento[] $apartamento
 * @property \App\Model\Entity\Mensalidade[] $mensalidade
 * @property \App\Model\Entity\Recurso[] $recurso
 * @property \App\Model\Entity\Convite[] $convite
 * @property \App\Model\Entity\Usuario[] $usuario
 */
class Condominio extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
