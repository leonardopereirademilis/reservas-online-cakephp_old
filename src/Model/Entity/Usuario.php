<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Usuario Entity
 *
 * @property int $id
 * @property string $account_expired
 * @property string $account_locked
 * @property string $email
 * @property string $enabled
 * @property string $nome
 * @property string $password
 * @property string $password_expired
 * @property string $username
 *
 * @property \App\Model\Entity\Convite[] $convite
 * @property \App\Model\Entity\Plano[] $plano
 * @property \App\Model\Entity\Reserva[] $reserva
 * @property \App\Model\Entity\Condominio[] $condominio
 * @property \App\Model\Entity\Papel[] $papel
 */
class Usuario extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
