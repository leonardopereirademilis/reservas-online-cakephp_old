<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Reserva Entity
 *
 * @property int $id
 * @property int $apartamento_id
 * @property string $aprovada
 * @property string $cancelada
 * @property string $comentario
 * @property \Cake\I18n\Time $data_aprovacao
 * @property \Cake\I18n\Time $data_cancelamento
 * @property \Cake\I18n\Time $data_solicitacao
 * @property int $recurso_id
 * @property int $usuario_id
 * @property float $valor
 * @property \Cake\I18n\Time $data_fim_evento
 * @property \Cake\I18n\Time $data_inicio_evento
 *
 * @property \App\Model\Entity\Apartamento $apartamento
 * @property \App\Model\Entity\Recurso $recurso
 * @property \App\Model\Entity\Usuario $usuario
 * @property \App\Model\Entity\Convidado[] $convidado
 */
class Reserva extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
