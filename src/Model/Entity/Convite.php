<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Convite Entity
 *
 * @property int $id
 * @property int $apartamento_id
 * @property \Cake\I18n\Time $data_aceite
 * @property \Cake\I18n\Time $data_convite
 * @property string $email
 * @property int $usuario_id
 * @property string $usuario_solicitou
 * @property string $aprovado
 *
 * @property \App\Model\Entity\Apartamento $apartamento
 * @property \App\Model\Entity\Usuario $usuario
 * @property \App\Model\Entity\Condominio[] $condominio
 */
class Convite extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
