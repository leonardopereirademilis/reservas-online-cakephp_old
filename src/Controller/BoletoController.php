<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Boleto Controller
 *
 * @property \App\Model\Table\BoletoTable $Boleto
 */
class BoletoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Mensalidade']
        ];
        $boleto = $this->paginate($this->Boleto);

        $this->set(compact('boleto'));
        $this->set('_serialize', ['boleto']);
    }

    /**
     * View method
     *
     * @param string|null $id Boleto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $boleto = $this->Boleto->get($id, [
            'contain' => ['Mensalidade']
        ]);

        $this->set('boleto', $boleto);
        $this->set('_serialize', ['boleto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $boleto = $this->Boleto->newEntity();
        if ($this->request->is('post')) {
            $boleto = $this->Boleto->patchEntity($boleto, $this->request->data);
            if ($this->Boleto->save($boleto)) {
                $this->Flash->success(__('The boleto has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The boleto could not be saved. Please, try again.'));
            }
        }
        $mensalidade = $this->Boleto->Mensalidade->find('list', ['limit' => 200]);
        $this->set(compact('boleto', 'mensalidade'));
        $this->set('_serialize', ['boleto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Boleto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $boleto = $this->Boleto->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $boleto = $this->Boleto->patchEntity($boleto, $this->request->data);
            if ($this->Boleto->save($boleto)) {
                $this->Flash->success(__('The boleto has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The boleto could not be saved. Please, try again.'));
            }
        }
        $mensalidade = $this->Boleto->Mensalidade->find('list', ['limit' => 200]);
        $this->set(compact('boleto', 'mensalidade'));
        $this->set('_serialize', ['boleto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Boleto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $boleto = $this->Boleto->get($id);
        if ($this->Boleto->delete($boleto)) {
            $this->Flash->success(__('The boleto has been deleted.'));
        } else {
            $this->Flash->error(__('The boleto could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
