<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Condominio Controller
 *
 * @property \App\Model\Table\CondominioTable $Condominio
 */
class CondominioController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Endereco', 'Plano']
        ];
        $condominio = $this->paginate($this->Condominio);

        $this->set(compact('condominio'));
        $this->set('_serialize', ['condominio']);
    }

    /**
     * View method
     *
     * @param string|null $id Condominio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $condominio = $this->Condominio->get($id, [
            'contain' => ['Endereco', 'Plano', 'Convite', 'Usuario', 'Apartamento', 'Mensalidade', 'Recurso']
        ]);

        $this->set('condominio', $condominio);
        $this->set('_serialize', ['condominio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $condominio = $this->Condominio->newEntity();
        if ($this->request->is('post')) {
            $condominio = $this->Condominio->patchEntity($condominio, $this->request->data);
            if ($this->Condominio->save($condominio)) {
                $this->Flash->success(__('The condominio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The condominio could not be saved. Please, try again.'));
            }
        }
        $endereco = $this->Condominio->Endereco->find('list', ['limit' => 200]);
        $plano = $this->Condominio->Plano->find('list', ['limit' => 200]);
        $convite = $this->Condominio->Convite->find('list', ['limit' => 200]);
        $usuario = $this->Condominio->Usuario->find('list', ['limit' => 200]);
        $this->set(compact('condominio', 'endereco', 'plano', 'convite', 'usuario'));
        $this->set('_serialize', ['condominio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Condominio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $condominio = $this->Condominio->get($id, [
            'contain' => ['Convite', 'Usuario']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $condominio = $this->Condominio->patchEntity($condominio, $this->request->data);
            if ($this->Condominio->save($condominio)) {
                $this->Flash->success(__('The condominio has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The condominio could not be saved. Please, try again.'));
            }
        }
        $endereco = $this->Condominio->Endereco->find('list', ['limit' => 200]);
        $plano = $this->Condominio->Plano->find('list', ['limit' => 200]);
        $convite = $this->Condominio->Convite->find('list', ['limit' => 200]);
        $usuario = $this->Condominio->Usuario->find('list', ['limit' => 200]);
        $this->set(compact('condominio', 'endereco', 'plano', 'convite', 'usuario'));
        $this->set('_serialize', ['condominio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Condominio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $condominio = $this->Condominio->get($id);
        if ($this->Condominio->delete($condominio)) {
            $this->Flash->success(__('The condominio has been deleted.'));
        } else {
            $this->Flash->error(__('The condominio could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
