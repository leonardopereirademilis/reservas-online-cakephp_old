<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Convite Controller
 *
 * @property \App\Model\Table\ConviteTable $Convite
 */
class ConviteController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Apartamento', 'Usuario']
        ];
        $convite = $this->paginate($this->Convite);

        $this->set(compact('convite'));
        $this->set('_serialize', ['convite']);
    }

    /**
     * View method
     *
     * @param string|null $id Convite id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $convite = $this->Convite->get($id, [
            'contain' => ['Apartamento', 'Usuario', 'Condominio']
        ]);

        $this->set('convite', $convite);
        $this->set('_serialize', ['convite']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $convite = $this->Convite->newEntity();
        if ($this->request->is('post')) {
            $convite = $this->Convite->patchEntity($convite, $this->request->data);
            if ($this->Convite->save($convite)) {
                $this->Flash->success(__('The convite has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The convite could not be saved. Please, try again.'));
            }
        }
        $apartamento = $this->Convite->Apartamento->find('list', ['limit' => 200]);
        $usuario = $this->Convite->Usuario->find('list', ['limit' => 200]);
        $condominio = $this->Convite->Condominio->find('list', ['limit' => 200]);
        $this->set(compact('convite', 'apartamento', 'usuario', 'condominio'));
        $this->set('_serialize', ['convite']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Convite id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $convite = $this->Convite->get($id, [
            'contain' => ['Condominio']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $convite = $this->Convite->patchEntity($convite, $this->request->data);
            if ($this->Convite->save($convite)) {
                $this->Flash->success(__('The convite has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The convite could not be saved. Please, try again.'));
            }
        }
        $apartamento = $this->Convite->Apartamento->find('list', ['limit' => 200]);
        $usuario = $this->Convite->Usuario->find('list', ['limit' => 200]);
        $condominio = $this->Convite->Condominio->find('list', ['limit' => 200]);
        $this->set(compact('convite', 'apartamento', 'usuario', 'condominio'));
        $this->set('_serialize', ['convite']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Convite id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $convite = $this->Convite->get($id);
        if ($this->Convite->delete($convite)) {
            $this->Flash->success(__('The convite has been deleted.'));
        } else {
            $this->Flash->error(__('The convite could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
