<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UsuarioPapel Controller
 *
 * @property \App\Model\Table\UsuarioPapelTable $UsuarioPapel
 */
class UsuarioPapelController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Papel', 'Usuario']
        ];
        $usuarioPapel = $this->paginate($this->UsuarioPapel);

        $this->set(compact('usuarioPapel'));
        $this->set('_serialize', ['usuarioPapel']);
    }

    /**
     * View method
     *
     * @param string|null $id Usuario Papel id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usuarioPapel = $this->UsuarioPapel->get($id, [
            'contain' => ['Papel', 'Usuario']
        ]);

        $this->set('usuarioPapel', $usuarioPapel);
        $this->set('_serialize', ['usuarioPapel']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usuarioPapel = $this->UsuarioPapel->newEntity();
        if ($this->request->is('post')) {
            $usuarioPapel = $this->UsuarioPapel->patchEntity($usuarioPapel, $this->request->data);
            if ($this->UsuarioPapel->save($usuarioPapel)) {
                $this->Flash->success(__('The usuario papel has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The usuario papel could not be saved. Please, try again.'));
            }
        }
        $papel = $this->UsuarioPapel->Papel->find('list', ['limit' => 200]);
        $usuario = $this->UsuarioPapel->Usuario->find('list', ['limit' => 200]);
        $this->set(compact('usuarioPapel', 'papel', 'usuario'));
        $this->set('_serialize', ['usuarioPapel']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Usuario Papel id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usuarioPapel = $this->UsuarioPapel->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usuarioPapel = $this->UsuarioPapel->patchEntity($usuarioPapel, $this->request->data);
            if ($this->UsuarioPapel->save($usuarioPapel)) {
                $this->Flash->success(__('The usuario papel has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The usuario papel could not be saved. Please, try again.'));
            }
        }
        $papel = $this->UsuarioPapel->Papel->find('list', ['limit' => 200]);
        $usuario = $this->UsuarioPapel->Usuario->find('list', ['limit' => 200]);
        $this->set(compact('usuarioPapel', 'papel', 'usuario'));
        $this->set('_serialize', ['usuarioPapel']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Usuario Papel id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usuarioPapel = $this->UsuarioPapel->get($id);
        if ($this->UsuarioPapel->delete($usuarioPapel)) {
            $this->Flash->success(__('The usuario papel has been deleted.'));
        } else {
            $this->Flash->error(__('The usuario papel could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
