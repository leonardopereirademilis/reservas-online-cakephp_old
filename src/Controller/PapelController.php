<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Papel Controller
 *
 * @property \App\Model\Table\PapelTable $Papel
 */
class PapelController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $papel = $this->paginate($this->Papel);

        $this->set(compact('papel'));
        $this->set('_serialize', ['papel']);
    }

    /**
     * View method
     *
     * @param string|null $id Papel id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $papel = $this->Papel->get($id, [
            'contain' => ['Usuario']
        ]);

        $this->set('papel', $papel);
        $this->set('_serialize', ['papel']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $papel = $this->Papel->newEntity();
        if ($this->request->is('post')) {
            $papel = $this->Papel->patchEntity($papel, $this->request->data);
            if ($this->Papel->save($papel)) {
                $this->Flash->success(__('The papel has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The papel could not be saved. Please, try again.'));
            }
        }
        $usuario = $this->Papel->Usuario->find('list', ['limit' => 200]);
        $this->set(compact('papel', 'usuario'));
        $this->set('_serialize', ['papel']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Papel id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $papel = $this->Papel->get($id, [
            'contain' => ['Usuario']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $papel = $this->Papel->patchEntity($papel, $this->request->data);
            if ($this->Papel->save($papel)) {
                $this->Flash->success(__('The papel has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The papel could not be saved. Please, try again.'));
            }
        }
        $usuario = $this->Papel->Usuario->find('list', ['limit' => 200]);
        $this->set(compact('papel', 'usuario'));
        $this->set('_serialize', ['papel']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Papel id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $papel = $this->Papel->get($id);
        if ($this->Papel->delete($papel)) {
            $this->Flash->success(__('The papel has been deleted.'));
        } else {
            $this->Flash->error(__('The papel could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
