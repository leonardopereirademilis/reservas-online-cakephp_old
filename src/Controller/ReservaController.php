<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Reserva Controller
 *
 * @property \App\Model\Table\ReservaTable $Reserva
 */
class ReservaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Apartamento', 'Recurso', 'Usuario']
        ];
        $reserva = $this->paginate($this->Reserva);

        $this->set(compact('reserva'));
        $this->set('_serialize', ['reserva']);
    }

    /**
     * View method
     *
     * @param string|null $id Reserva id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $reserva = $this->Reserva->get($id, [
            'contain' => ['Apartamento', 'Recurso', 'Usuario', 'Convidado']
        ]);

        $this->set('reserva', $reserva);
        $this->set('_serialize', ['reserva']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reserva = $this->Reserva->newEntity();
        if ($this->request->is('post')) {
            $reserva = $this->Reserva->patchEntity($reserva, $this->request->data);
            if ($this->Reserva->save($reserva)) {
                $this->Flash->success(__('The reserva has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The reserva could not be saved. Please, try again.'));
            }
        }
        $apartamento = $this->Reserva->Apartamento->find('list', ['limit' => 200]);
        $recurso = $this->Reserva->Recurso->find('list', ['limit' => 200]);
        $usuario = $this->Reserva->Usuario->find('list', ['limit' => 200]);
        $this->set(compact('reserva', 'apartamento', 'recurso', 'usuario'));
        $this->set('_serialize', ['reserva']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Reserva id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $reserva = $this->Reserva->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reserva = $this->Reserva->patchEntity($reserva, $this->request->data);
            if ($this->Reserva->save($reserva)) {
                $this->Flash->success(__('The reserva has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The reserva could not be saved. Please, try again.'));
            }
        }
        $apartamento = $this->Reserva->Apartamento->find('list', ['limit' => 200]);
        $recurso = $this->Reserva->Recurso->find('list', ['limit' => 200]);
        $usuario = $this->Reserva->Usuario->find('list', ['limit' => 200]);
        $this->set(compact('reserva', 'apartamento', 'recurso', 'usuario'));
        $this->set('_serialize', ['reserva']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Reserva id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $reserva = $this->Reserva->get($id);
        if ($this->Reserva->delete($reserva)) {
            $this->Flash->success(__('The reserva has been deleted.'));
        } else {
            $this->Flash->error(__('The reserva could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
