<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ReservaController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ReservaController Test Case
 */
class ReservaControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.reserva',
        'app.apartamento',
        'app.condominio',
        'app.endereco',
        'app.cidade',
        'app.estado',
        'app.pais',
        'app.plano',
        'app.tipo_plano',
        'app.usuario',
        'app.convite',
        'app.condominio_convite',
        'app.condominio_usuario',
        'app.papel',
        'app.usuario_papel',
        'app.mensalidade',
        'app.boleto',
        'app.recurso',
        'app.unidade_tempo_reserva',
        'app.cor',
        'app.imagem',
        'app.indisponibilidade',
        'app.tipo_reserva',
        'app.recurso_tipo_reserva',
        'app.convidado'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
