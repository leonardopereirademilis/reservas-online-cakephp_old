<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsuarioPapelTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsuarioPapelTable Test Case
 */
class UsuarioPapelTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsuarioPapelTable
     */
    public $UsuarioPapel;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.usuario_papel',
        'app.papel',
        'app.usuario',
        'app.convite',
        'app.apartamento',
        'app.condominio',
        'app.endereco',
        'app.cidade',
        'app.estado',
        'app.pais',
        'app.plano',
        'app.tipo_plano',
        'app.mensalidade',
        'app.boleto',
        'app.recurso',
        'app.unidade_tempo_reserva',
        'app.cor',
        'app.imagem',
        'app.indisponibilidade',
        'app.reserva',
        'app.convidado',
        'app.tipo_reserva',
        'app.recurso_tipo_reserva',
        'app.condominio_convite',
        'app.condominio_usuario'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UsuarioPapel') ? [] : ['className' => 'App\Model\Table\UsuarioPapelTable'];
        $this->UsuarioPapel = TableRegistry::get('UsuarioPapel', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsuarioPapel);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
