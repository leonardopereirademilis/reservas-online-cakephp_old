<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ImagemTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ImagemTable Test Case
 */
class ImagemTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ImagemTable
     */
    public $Imagem;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.imagem',
        'app.recurso',
        'app.condominio',
        'app.endereco',
        'app.cidade',
        'app.estado',
        'app.pais',
        'app.plano',
        'app.tipo_plano',
        'app.usuario',
        'app.convite',
        'app.apartamento',
        'app.reserva',
        'app.convidado',
        'app.condominio_convite',
        'app.condominio_usuario',
        'app.papel',
        'app.usuario_papel',
        'app.mensalidade',
        'app.boleto',
        'app.unidade_tempo_reserva',
        'app.cor',
        'app.indisponibilidade',
        'app.tipo_reserva',
        'app.recurso_tipo_reserva'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Imagem') ? [] : ['className' => 'App\Model\Table\ImagemTable'];
        $this->Imagem = TableRegistry::get('Imagem', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Imagem);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
