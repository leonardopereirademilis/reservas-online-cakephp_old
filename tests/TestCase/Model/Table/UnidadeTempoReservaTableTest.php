<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UnidadeTempoReservaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UnidadeTempoReservaTable Test Case
 */
class UnidadeTempoReservaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UnidadeTempoReservaTable
     */
    public $UnidadeTempoReserva;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.unidade_tempo_reserva',
        'app.recurso',
        'app.condominio',
        'app.endereco',
        'app.cidade',
        'app.estado',
        'app.pais',
        'app.plano',
        'app.tipo_plano',
        'app.usuario',
        'app.convite',
        'app.apartamento',
        'app.reserva',
        'app.convidado',
        'app.condominio_convite',
        'app.condominio_usuario',
        'app.papel',
        'app.usuario_papel',
        'app.mensalidade',
        'app.boleto',
        'app.cor',
        'app.imagem',
        'app.indisponibilidade',
        'app.tipo_reserva',
        'app.recurso_tipo_reserva'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UnidadeTempoReserva') ? [] : ['className' => 'App\Model\Table\UnidadeTempoReservaTable'];
        $this->UnidadeTempoReserva = TableRegistry::get('UnidadeTempoReserva', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UnidadeTempoReserva);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
