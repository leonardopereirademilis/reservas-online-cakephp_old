<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConvidadoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConvidadoTable Test Case
 */
class ConvidadoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConvidadoTable
     */
    public $Convidado;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.convidado',
        'app.reserva',
        'app.apartamento',
        'app.condominio',
        'app.endereco',
        'app.cidade',
        'app.estado',
        'app.pais',
        'app.plano',
        'app.tipo_plano',
        'app.usuario',
        'app.convite',
        'app.condominio_convite',
        'app.condominio_usuario',
        'app.papel',
        'app.usuario_papel',
        'app.mensalidade',
        'app.boleto',
        'app.recurso',
        'app.unidade_tempo_reserva',
        'app.cor',
        'app.imagem',
        'app.indisponibilidade',
        'app.tipo_reserva',
        'app.recurso_tipo_reserva'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Convidado') ? [] : ['className' => 'App\Model\Table\ConvidadoTable'];
        $this->Convidado = TableRegistry::get('Convidado', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Convidado);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
