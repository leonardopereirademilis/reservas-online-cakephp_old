<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IndisponibilidadeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IndisponibilidadeTable Test Case
 */
class IndisponibilidadeTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IndisponibilidadeTable
     */
    public $Indisponibilidade;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.indisponibilidade',
        'app.recurso',
        'app.condominio',
        'app.endereco',
        'app.cidade',
        'app.estado',
        'app.pais',
        'app.plano',
        'app.tipo_plano',
        'app.usuario',
        'app.convite',
        'app.apartamento',
        'app.reserva',
        'app.convidado',
        'app.condominio_convite',
        'app.condominio_usuario',
        'app.papel',
        'app.usuario_papel',
        'app.mensalidade',
        'app.boleto',
        'app.unidade_tempo_reserva',
        'app.cor',
        'app.imagem',
        'app.tipo_reserva',
        'app.recurso_tipo_reserva'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Indisponibilidade') ? [] : ['className' => 'App\Model\Table\IndisponibilidadeTable'];
        $this->Indisponibilidade = TableRegistry::get('Indisponibilidade', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Indisponibilidade);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
