<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TipoPlanoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TipoPlanoTable Test Case
 */
class TipoPlanoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TipoPlanoTable
     */
    public $TipoPlano;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tipo_plano',
        'app.plano',
        'app.usuario',
        'app.convite',
        'app.apartamento',
        'app.condominio',
        'app.endereco',
        'app.cidade',
        'app.estado',
        'app.pais',
        'app.mensalidade',
        'app.boleto',
        'app.recurso',
        'app.unidade_tempo_reserva',
        'app.cor',
        'app.imagem',
        'app.indisponibilidade',
        'app.reserva',
        'app.convidado',
        'app.tipo_reserva',
        'app.recurso_tipo_reserva',
        'app.condominio_convite',
        'app.condominio_usuario',
        'app.papel',
        'app.usuario_papel'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TipoPlano') ? [] : ['className' => 'App\Model\Table\TipoPlanoTable'];
        $this->TipoPlano = TableRegistry::get('TipoPlano', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TipoPlano);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
